Files part of this demo must be copied to a (local) web server and accessed with a browser not to be subject to Flash sandbox security restrictions.

For more information, see
http://help.adobe.com/en_US/ActionScript/3.0_ProgrammingAS3/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3f.html
