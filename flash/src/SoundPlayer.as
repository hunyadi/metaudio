/**
* @file
* @brief    metaudio JavaScript-driven Flash audio player
* @author   Levente Hunyadi
* @version  1.0
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

/*
* metaudio JavaScript-driven Flash audio player
* Copyright 2010 Levente Hunyadi
*
* metaudio player is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* metaudio player is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with metaudio player.  If not, see <http://www.gnu.org/licenses/>.
*/ 

package {
	import flash.events.EventDispatcher;

	/**
	 * A sound object.
	 * @author Levente Hunyadi
	 */
	internal class SoundPlayer extends EventDispatcher {
		/**
		 * Starts playing a sound if it is not already playing.
		 */
		public function play() : void {
			
		}
		
		/**
		 * Pauses a sound that is playing.
		 */
		public function pause() : void {
			
		}
		
		/**
		 * Resumes a sound that has been paused.
		 */
		public function resume() : void {
			
		}
		
		/**
		 * Stops playing a sound.
		 */
		public function stop() : void {
			
		}
		
		/**
		 * Total size of sound recording.
		 */
		public function get bytesTotal() : int {
			return 0;
		}
		
		/**
		 * Loaded size of sound recording.
		 */
		public function get bytesLoaded() : int {
			return 0;
		}

		/**
		 * The total play time of the sound [ms].
		 */
		public function get duration() : int {
			return 0;
		}

		/**
		 * Playhead position [ms].
		 */
		public function get position() : int {
			return 0;
		}
		
		/**
		 * Playhead position [ms].
		 */
		public function set position(pos:int) : void {
			
		}
	}
}