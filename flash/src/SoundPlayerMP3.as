/**
* @file
* @brief    metaudio JavaScript-driven Flash audio player
* @author   Levente Hunyadi
* @version  1.0
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

/*
* metaudio JavaScript-driven Flash audio player
* Copyright 2010 Levente Hunyadi
*
* metaudio player is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* metaudio player is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with metaudio player.  If not, see <http://www.gnu.org/licenses/>.
*/ 

package {
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundLoaderContext;
	import flash.net.URLRequest;

	/**
	 * An MP3 sound object.
	 * @author Levente Hunyadi
	 */
	internal class SoundPlayerMP3 extends SoundPlayer {
		/** Original URL used to construct the instance. */
		private var _url : String;
		private var _sound : Sound;
		private var _soundChannel : SoundChannel;
		/** Playhead position when the sound was paused [ms]. */
		private var _pausedAt : int = 0;
		
		public function SoundPlayerMP3(url:String) {
			_url = url;
			_sound = new Sound(new URLRequest(url), new SoundLoaderContext(1000, true));
			_sound.addEventListener(Event.COMPLETE, function () : void {
				dispatchEvent(new SoundPlayerEvent(SoundPlayerEvent.LOAD, _url));
			});
		}
		
		/**
		 * Starts playing a sound at the specified playhead position.
		 */
		private function playSound(pos:int = 0) : void {
			_soundChannel = _sound.play(pos);
			_soundChannel.addEventListener(Event.SOUND_COMPLETE, function () : void {
				dispatchEvent(new SoundPlayerEvent(SoundPlayerEvent.FINISH, _url));
				_soundChannel = null;
			});
		}
		
		public override function play() : void {
			if (!_soundChannel) {
				playSound();
				dispatchEvent(new SoundPlayerEvent(SoundPlayerEvent.PLAY, _url));
			}
		}
		
		public override function pause() : void {
			if (_soundChannel) {
				_pausedAt = _soundChannel.position;
				_soundChannel.stop();
				_soundChannel = null;
				dispatchEvent(new SoundPlayerEvent(SoundPlayerEvent.PAUSE, _url));
			}
		}
		
		public override function resume() : void {
			if (!_soundChannel) {
				playSound(_pausedAt);
				dispatchEvent(new SoundPlayerEvent(SoundPlayerEvent.RESUME, _url));
			}
		}
		
		public override function stop() : void {
			if (_soundChannel) {
				_soundChannel.stop();
				_soundChannel = null;
				_pausedAt = 0;
				dispatchEvent(new SoundPlayerEvent(SoundPlayerEvent.STOP, _url));
			}
		}
		
		public override function get bytesTotal() : int {
			return _sound.bytesTotal;
		}

		public override function get bytesLoaded() : int {
			return _sound.bytesLoaded;
		}

		public override function get duration() : int {
			return _sound.length;
		}
		
		public override function get position() : int {
			if (_soundChannel) {
				return _soundChannel.position;
			} else {
				return _pausedAt;
			}
		}
		
		public override function set position(pos:int) : void {
			if (_soundChannel) {
				_soundChannel.stop();
				playSound(pos);
			} else {
				_pausedAt = pos;
			}
			dispatchEvent(new SoundPlayerEvent(SoundPlayerEvent.SEEK, _url, pos));
		}
	}
}