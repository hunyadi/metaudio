/**
* @file
* @brief    metaudio JavaScript-driven Flash audio player
* @author   Levente Hunyadi
* @version  1.0
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

/*
* metaudio JavaScript-driven Flash audio player
* Copyright 2010 Levente Hunyadi
*
* metaudio player is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* metaudio player is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with metaudio player.  If not, see <http://www.gnu.org/licenses/>.
*/ 

package {
	import flash.events.NetStatusEvent;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	
	/**
	 * An MP4/AAC sound object.
	 * @author Levente Hunyadi
	 */
	internal class SoundPlayerAAC extends SoundPlayer {
		private var _url : String;
		private var _stream : NetStream;
		private var _conn : NetConnection;
		private var _duration : Number = 0;
		
		public function SoundPlayerAAC(url:String) {
			_conn = new NetConnection();
			_conn.connect(null);
			_url = url;
		}
		
		public override function play() : void {
			if (!_stream) {
				_stream = new NetStream(_conn);
				_stream.inBufferSeek = true;
				_stream.client = {
					onMetaData: function (infoObject:Object) : void {
						if (infoObject['duration']) {
							_duration = infoObject['duration'];
						}
						dispatchEvent(new SoundPlayerEvent(SoundPlayerEvent.LOAD, _url));
					}
				}
				_stream.addEventListener(NetStatusEvent.NET_STATUS, function (event:NetStatusEvent) : void {
					switch (event.info.code) {
						case 'NetStream.Play.Stop':
							dispatchEvent(new SoundPlayerEvent(SoundPlayerEvent.FINISH, _url));
							_stream = null;
							break;
					}
				});
				_stream.play(_url);
				dispatchEvent(new SoundPlayerEvent(SoundPlayerEvent.PLAY, _url));
			}
		}
		
		public override function pause() : void {
			if (_stream) {
				_stream.pause();
				dispatchEvent(new SoundPlayerEvent(SoundPlayerEvent.PAUSE, _url));
			}
		}
		
		public override function resume() : void {
			if (_stream) {
				_stream.resume();
				dispatchEvent(new SoundPlayerEvent(SoundPlayerEvent.RESUME, _url));
			}
		}
		
		public override function stop() : void {
			if (_stream) {
				_stream.close();
				_stream = null;
				dispatchEvent(new SoundPlayerEvent(SoundPlayerEvent.STOP, _url));
			}
		}
		
		public override function get bytesTotal() : int {
			return _stream ? _stream.bytesTotal : 0;
		}

		public override function get bytesLoaded() : int {
			return _stream ? _stream.bytesLoaded : 0;
		}

		public override function get duration() : int {
			return 1000 * _duration;
		}
		
		public override function get position() : int {
			if (_stream) {
				return 1000 * _stream.time;
			} else {
				return 0;
			}
		}
		
		public override function set position(pos:int) : void {
			if (_stream) {
				_stream.seek(0.001 * pos);
				dispatchEvent(new SoundPlayerEvent(SoundPlayerEvent.SEEK, _url, pos));
			}
		}
	}
}