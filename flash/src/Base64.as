/*
Base64 - 1.1.0

Copyright (c) 2006 Steve Webster

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package {
	import flash.utils.ByteArray;

	public class Base64 {
		private static const BASE64_CHARS : String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

		public static function decode(data : String) : String {
			// decode data to ByteArray
			var bytes : ByteArray = decodeToByteArray(data);

			// convert to string
			return bytes.readUTFBytes(bytes.length);
		}

		private static function decodeToByteArray(data : String) : ByteArray {
			// initialise output ByteArray for decoded data
			var output : ByteArray = new ByteArray();

			// create data and output buffers
			var dataBuffer : Array = new Array(4);
			var outputBuffer : Array = new Array(3);

			for (var i : uint = 0; i < data.length; i += 4) {
				// populate data buffer with position of Base64 characters for next 4 bytes from encoded data
				for (var j : uint = 0; j < 4 && i + j < data.length; j++) {
					dataBuffer[j] = BASE64_CHARS.indexOf(data.charAt(i + j));
				}

				// decode data buffer back into bytes
				outputBuffer[0] = (dataBuffer[0] << 2) | ((dataBuffer[1] & 0x30) >> 4);
				outputBuffer[1] = ((dataBuffer[1] & 0x0f) << 4) | ((dataBuffer[2] & 0x3c) >> 2);
				outputBuffer[2] = ((dataBuffer[2] & 0x03) << 6) | dataBuffer[3];

				// add all non-padded bytes in output buffer to decoded data
				for (var k : uint = 0; k < outputBuffer.length; k++) {
					if (dataBuffer[k + 1] == 64)
						break;
					output.writeByte(outputBuffer[k]);
				}
			}

			output.position = 0;  // rewind decoded data ByteArray
			return output;
		}
	}
}