/**
* @file
* @brief    metaudio JavaScript-driven Flash audio player
* @author   Levente Hunyadi
* @version  1.0
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

/*
* metaudio JavaScript-driven Flash audio player
* Copyright 2010 Levente Hunyadi
*
* metaudio player is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* metaudio player is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with metaudio player.  If not, see <http://www.gnu.org/licenses/>.
*/ 

package {
	import flash.events.Event;
	
	/**
	 * Fired when the play status of a sound changes.
	 * @author Levente Hunyadi
	 */
	public class SoundPlayerEvent extends Event {
		public static const LOAD : String = 'load';
		public static const PLAY : String = 'play';
		public static const PAUSE : String = 'pause';
		public static const RESUME : String = 'resume';
		public static const STOP : String = 'stop';
		public static const FINISH : String = 'finish';
		public static const SEEK : String = 'seek'
		
		private var _url : String;
		private var _pos : int;
		
		/**
		 * The URL used to construct the sound object the event applies to.
		 */
		public function get url() : String {
			return _url;
		}
		
		/**
		 * The playhead position when the event occurred.
		 * The value is only meaningful for pausing, resuming and seeking within a sound.
		 */
		public function get position() : int {
			return _pos;
		}
		
		public function SoundPlayerEvent(type:String, url:String, pos:int = 0) {
			super(type);
			_url = url;
			_pos = pos;
		}
	}
}