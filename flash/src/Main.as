/**
* @file
* @brief    metaudio JavaScript-driven Flash audio player
* @author   Levente Hunyadi
* @version  1.0
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

/*
* metaudio JavaScript-driven Flash audio player
* Copyright 2010 Levente Hunyadi
*
* metaudio player is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* metaudio player is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with metaudio player.  If not, see <http://www.gnu.org/licenses/>.
*/ 

package {
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.external.ExternalInterface;
	import flash.media.SoundMixer;
	import flash.utils.ByteArray;
	
	/**
	 * metaudio: a JavaScript-driven sound player.
	 * @author Levente Hunyadi
	 */
	public class Main extends Sprite 
	{
		private var _sounds : Array /* Array<SoundPlayer> */ = new Array();

		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function log(message : String, severity : String = 'log') : void {
			if (ExternalInterface.available) {
				ExternalInterface.call('console.' + severity, '[metaudio] ' + message);
			} else {
				trace('[metaudio]', severity, message);
			}
		}
		
		private function propagateEvent(event:SoundPlayerEvent) : void {
			if (ExternalInterface.available) {
				ExternalInterface.call('metaudio.on' + event.type, event.url, event.position);
			}
		}
		
		private function create(url : String) : void {
			var pattern : RegExp = new RegExp('[.](aac|m4a)$');
			var sound : SoundPlayer;
			if (pattern.test(url)) {
				sound = new SoundPlayerAAC(url);
				log('AAC sound stream created.');
			} else {
				sound = new SoundPlayerMP3(url);
				log('MP3 sound stream created.');
			}
			_sounds[url] = sound;
			sound.addEventListener(SoundPlayerEvent.LOAD, function (event:SoundPlayerEvent) : void {
				log('Loaded <' + event.url + '>.', 'info');
				propagateEvent(event);
			});
			sound.addEventListener(SoundPlayerEvent.PLAY, function (event:SoundPlayerEvent) : void {
				log('Playing <' + event.url + '>.', 'info');
				propagateEvent(event);
			});
			sound.addEventListener(SoundPlayerEvent.STOP, function (event:SoundPlayerEvent) : void {
				log('Stopped <' + event.url + '>.', 'info');
				propagateEvent(event);
			});
			sound.addEventListener(SoundPlayerEvent.RESUME, function (event:SoundPlayerEvent) : void {
				log('Resuming <' + event.url + '>.', 'info');
				propagateEvent(event);
			});
			sound.addEventListener(SoundPlayerEvent.PAUSE, function (event:SoundPlayerEvent) : void {
				log('Paused <' + event.url + '>.', 'info');
				propagateEvent(event);
			});
			sound.addEventListener(SoundPlayerEvent.FINISH, function (event:SoundPlayerEvent) : void {
				log('Finished <' + event.url + '>.', 'info');
				propagateEvent(event);
			});
			sound.addEventListener(SoundPlayerEvent.SEEK, function (event:SoundPlayerEvent) : void {
				var milliseconds : int =  event.position;
				var seconds : int = milliseconds / 1000;
				var minutes : int = seconds / 60;
				seconds = seconds % 60;
				
				var time : String;
				if (seconds < 10) {
					time = minutes + ':0' + seconds;
				} else {
					time = minutes + ':' + seconds;
				}
				log('Seeking <' + event.url + '> to ' + time + ' (' + milliseconds + ' ms).', 'log');
				propagateEvent(event);
			});
		}

		private function init(e:Event = null) : void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			log('Flash player initialized.', 'info');
			if (ExternalInterface.available) {
				if (ExternalInterface.objectID != null) {
					// add the prefix "mp" to avoid name clashes with ActiveX reserved words, e.g. "play" is a reserved word
					ExternalInterface.addCallback('mpCreate', create);
					ExternalInterface.addCallback('mpPlay', function (url : String) : void {
						if (_sounds[url]) {
							_sounds[url].play();
						}
					});
					ExternalInterface.addCallback('mpPause', function (url : String) : void {
						if (_sounds[url]) {
							_sounds[url].pause();
						}
					});
					ExternalInterface.addCallback('mpResume', function (url : String) : void {
						if (_sounds[url]) {
							_sounds[url].resume();
						}
					});
					ExternalInterface.addCallback('mpStop', function (url : String) : void {
						if (_sounds[url]) {
							_sounds[url].stop();
						}
					});
					ExternalInterface.addCallback('mpGetBytesTotal', function (url : String) : int {
						if (_sounds[url]) {
							return _sounds[url].bytesTotal;  // length in milliseconds
						} else {
							return 0;
						}
					});
					ExternalInterface.addCallback('mpGetBytesLoaded', function (url : String) : int {
						if (_sounds[url]) {
							return _sounds[url].bytesLoaded;  // length in milliseconds
						} else {
							return 0;
						}
					});
					ExternalInterface.addCallback('mpGetDuration', function (url : String) : int {
						if (_sounds[url]) {
							return _sounds[url].duration;  // length in milliseconds
						} else {
							return 0;
						}
					});
					ExternalInterface.addCallback('mpGetPosition', function (url : String) : int {
						if (_sounds[url]) {
							return _sounds[url].position;  // playhead position in milliseconds
						} else {
							return 0;
						}
					});
					ExternalInterface.addCallback('mpSetPosition', function (url : String, pos:int) : void {
						if (_sounds[url]) {
							_sounds[url].position = pos;
						}
					});
					ExternalInterface.addCallback('mpGetPeak', function () : Object {
						if (SoundMixer.areSoundsInaccessible()) {
							return false;  // another sound is playing that is in a different security sandbox
						} else {
							var bytes : ByteArray = new ByteArray();
							SoundMixer.computeSpectrum(bytes, true, 0);  // sample wave data at 44.1 KHz, compute FFT
							
							var len : int = bytes.length / (2 * 4);  // two channels times 4 bytes per float
							var k : int;
							
							// get 256 values per channel
							var leftdata : Number = 0;
							for (k = 0; k < len; k += 8) {  // use loop unrolling
								leftdata = Math.max(leftdata,  // data in range [0;2]
									bytes.readFloat(), bytes.readFloat(), bytes.readFloat(), bytes.readFloat(),
									bytes.readFloat(), bytes.readFloat(), bytes.readFloat(), bytes.readFloat()
								);
							}
							var rightdata : Number = 0;
							for (k = 0; k < len; k += 8) {  // use loop unrolling
								rightdata = Math.max(rightdata,  // data in range [0;2]
									bytes.readFloat(), bytes.readFloat(), bytes.readFloat(), bytes.readFloat(),
									bytes.readFloat(), bytes.readFloat(), bytes.readFloat(), bytes.readFloat()
								);
							}
							return {
								left: leftdata / 2,  // coerce data in range [0;1]
								right: rightdata / 2
							};
						}
					});
					ExternalInterface.addCallback('mpGetWaveform', function () : Object {
						if (SoundMixer.areSoundsInaccessible()) {
							return false;
						} else {
							var bytes : ByteArray = new ByteArray();
							SoundMixer.computeSpectrum(bytes, false, 0);  // sample wave data at 44.1 KHz
							
							var len : int = bytes.length / (2 * 4);
							var k : int;
							
							// get 256 values per channel
							var leftdata : Array = [];
							for (k = 0; k < len; k++) {
								leftdata.push(bytes.readFloat());  // data in range [-1;1]
							}
							var rightdata : Array = [];
							for (k = 0; k < len; k++) {
								rightdata.push(bytes.readFloat());
							}
							return {
								left: leftdata,
								right: rightdata
							};
						}
					});
					log('Flash player event callbacks registered.', 'info');
				} else {
					log('Flash player missing object id, event callbacks not registered.', 'error');
				}
			}
			
			/*
			var url : String = 'message-in-a-bottle.m4a';
			create(url);
			var sound : SoundPlayer = _sounds[url];
			sound.play();
			*/
		}
	}
}