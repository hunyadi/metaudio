/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010-2012 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

/*
* metaudio Player: JavaScript sound player for mp3/m4a audio
* Copyright 2010-2012 Levente Hunyadi
*
* metaudio is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* metaudio is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with metaudio.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
* Requires MooTools Core 1.2 or later.
*/

/**
* Graphical user interface for metaudio Player.
*/
function metaudioPlayer(settings) {
	// --- EDIT OPTIONS BELOW TO MODIFY DEFAULTS --- //
	// set default configuration options
	settings = Object.merge({}, settings);
	// --- END OF DEFAULT OPTIONS --- //

	/**
	* Adds graphical user interface elements to the HTML document.
	*/
	document.addEvent('domready', function () {
		// initialize Flash
		window.metaudio.init(settings);

		if (typeof(window.metaudio.create) == 'undefined') {  // metaudio not initialized, check Flash version at http://www.adobe.com/software/flash/about/
			return;
		}

		// add control interface for each playable sound object
		var anchors = document.getElements('a.metaudio-player');
		anchors.each(function (anchor) {
			anchor.addClass('sm2_link').addEvent('click', function (event) {
				var url = anchor.href;
				var sound = window.metaudio.sounds[url];
				if (!sound) {
					// create sound object
					// bind user actions to sound actions and sound events to user interface changes
					sound = window.metaudio.create({
						url: url,
						usePeakData: false,
						useWaveformData: false,
						onplay: function () {
							anchor.addClass('sm2_playing');
						},
						onstop: function () {
							anchor.removeClass('sm2_playing').removeClass('sm2_paused');
						},
						onpause: function () {
							anchor.removeClass('sm2_playing');
							anchor.addClass('sm2_paused');
						},
						onresume: function () {
							anchor.removeClass('sm2_paused');
							anchor.addClass('sm2_playing');
						},
						onfinish: function () {
							anchor.removeClass('sm2_playing').removeClass('sm2_paused');
						}
					});
				}

				// stop playing all recordings except the one activated
				window.metaudio.pauseAllBut(url);
				
				// pause sound currently playing or resume sound paused previously
				if (anchor.hasClass('sm2_paused')) {  // sound playing or paused
					sound.resume();
				} else if (anchor.hasClass('sm2_playing')) {
					sound.pause();
				} else {  // sound not yet loaded or stopped
					sound.play();
				}
				
				// suppress default anchor behavior
				return false;  // prevent clicks on sound links taking user away from page
			});
		});

		if (document.all) {  // Internet Explorer only
			/**
			* Address a Flash unloading bug in IE.
			*/
			function _removeSoundPlayer() {
				var el = document.all['metaudio-player'];
				if (el) {
					el.removeNode(true);
				}
			}

			if (window.addEventListener) {
				window.addEventListener('unload', _removeSoundPlayer, false);
			} else if (window.attachEvent) {
				window.attachEvent('onunload', _removeSoundPlayer);
			} else {
				window.unload = _removeSoundPlayer;
			}
		}
	});
}
