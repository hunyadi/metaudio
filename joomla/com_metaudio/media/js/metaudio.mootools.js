/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010-2012 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

/*
* metaudio Player: JavaScript sound player for mp3/m4a audio
* Copyright 2010-2012 Levente Hunyadi
*
* metaudio is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* metaudio is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with metaudio.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
* Requires MooTools Core 1.2 or later.
*/

/**
* Interface for metaudio Player.
* The initial interface comprises of a single method to initialize the player, "init". Successful
* initialization exposes the method "create" to create sound objects, and a collection "sounds"
* to iterate over sound objects. Sound objects are backed by a player based on either the HTML5
* audio element, or Flash. The internal implementation is opaque to the user, the same sound object
* interface is exposed:
* # url: string
* # player: unspecified-object
* # play: function () : void
* # stop: function () : void
* # resume: function () : void
* # pause: function () : void
* # getTotal: function () : unspecified-float-type
* # getLoaded: function () : unspecified-float-type
* # getDuration: function () : milliseconds
* # getPosition: function () : milliseconds
* # setPosition: function (milliseconds) : void
*/
window.metaudio = {
	init: function (settings) {
		function addSoundEvents(sound, attrs) {
			var noop = function () { };
			var soundevents = {
				onload: noop,
				onplay: noop,
				onstop: noop,
				onresume: noop,
				onpause: noop,
				onfinish: noop,
				onseek: noop,
				whileplaying: noop,
				whileloading: noop
			};
			for (var evt in soundevents) {
				sound[evt] = attrs[evt] ? attrs[evt] : soundevents[evt];
			}
		}

		// helper element to test for HTML <audio> capabilities
		var html5audio = new Element('audio');
		if (html5audio && html5audio.canPlayType) {
			// add support for events specific to HTML element <audio>
			['progress','canplay','canplaythrough','play','pause','ended','seeking','timeupdate'].each(function (evt) {
				Element.NativeEvents[evt] = 1;
			});
		} else {
			html5audio = false;
		}

		var swf = new Swiff(settings.swfurl, {
			id: 'metaudio-player',
			container: 'metaudio-placeholder',
			params: {
				menu: false,
				scale: 'noScale'
			},
			properties: {
				name: 'metaudio-player'  // added for IE compatibility
			}
		});

		if (swf) {
			swf = document.id(swf);  // extend with MooTools methods
		}

		window['metaudio'] = {  // replace metaudio global object
			/**
			* Sounds collection.
			*/
			'sounds': {},

			html5player: {
				/**
				* Creates a new sound instance and adds it to the sounds collection.
				*/
				'create': function (attrs) {
					var player = new Element('audio', {
						src: attrs.url
					});

					// create new sound instance
					var sound = {
						'url': attrs.url,
						/** An HTML <audio> element. */
						'player': player,
						/** True when the sound has been paused and is waiting to be resumed. */
						'paused': false,

						'play': function () {
							player.play();
						},
						'stop': function () {
							player.pause();
						},
						'resume': function () {
							player.play();
						},
						'pause': function () {
							player.pause();
						},
						'getTotal': function () {
							return sound.duration;
						},
						'getLoaded': function () {
							var d = sound.duration;
							if (d) {  // duration is available
								d = 0;
								for (range in sound.buffered) {
									d += range;
								}
							}
							return d;
						},
						'getDuration': function () {
							return player.duration * 1000;
						},
						'getPosition': function () {
							return player.currentTime * 1000;
						},
						'setPosition': function (milliseconds) {
							player.currentTime = milliseconds / 1000;
						}
					};
					addSoundEvents(sound, attrs);

					player.addEvents({
						'progress': function () {
							sound.whileloading();
						},
						/**
						* Fired when a sound has been buffered and is ready to play.
						*/
						'canplay': function () {
							sound.onload();
						},
						/**
						* Fired when a sound has been fully buffered.
						*/
						'canplaythrough': function () {
							sound.whileloading();  // loading is finished
						},
						/**
						* Fired when a sound starts playing or a sound that has been paused resumes playing.
						*/
						'play': function () {
							if (sound.paused) {
								sound.paused = false;
								sound.onresume();
							} else {
								sound.onplay();
							}
						},
						/**
						* Fired when a sound stops playing or a sound that has started playing is paused.
						*/
						'pause': function () {
							sound.paused = true;
							sound.onpause();
						},
						/**
						* Fired when a sound completes playing.
						*/
						'ended': function () {
							sound.paused = false;
							sound.onfinish();
						},
						/**
						* Fired when a new playhead position is set.
						*/
						'seeking': function () {
							sound.onseek(sound.currentTime * 1000);
						},
						/**
						* Fired when the playhead position has changed.
						*/
						'timeupdate': function () {
							sound.whileplaying();
						}
					});

					return sound;
				}
			},

			flashplayer: {
				/**
				* HTML DOM object of the global Flash player instance.
				*/
				'player': swf,
				/**
				* Creates a new sound instance and adds it to the sounds collection.
				*/
				'create': function (attrs) {
					var self = this;
					var player = this.player;

					// create new sound instance
					player.mpCreate(attrs.url);

					var sound = {
						'url': attrs.url,
						'loading': true,
						'playing': false,

						'play': function () {
							player.mpPlay(this.url);
						},
						'stop': function () {
							player.mpStop(this.url);
						},
						'resume': function () {
							player.mpResume(this.url);
						},
						'pause': function () {
							player.mpPause(this.url);
						},
						'getTotal': function () {
							return player.mpGetBytesTotal(this.url);
						},
						'getLoaded': function () {
							return player.mpGetBytesLoaded(this.url);
						},
						'getDuration': function () {
							return player.mpGetDuration(this.url);
						},
						'getPosition': function () {
							return player.mpGetPosition(this.url);
						},
						'setPosition': function (milliseconds) {
							player.mpSetPosition(this.url, milliseconds);
						},
						/**
						* Retrieves peak volume data and saves it in a property.
						*/
						'getPeak': function () {
							return player.mpGetPeak();
						},
						/**
						* Retrieves waveform data and saves it in a property.
						*/
						'getWaveform': function () {
							return player.mpGetWaveform();
						}
					};
					addSoundEvents(sound, attrs);

					window.clearInterval(this.interval);
					this.interval = window.setInterval(function () {
						self.poll();
					}, 100);

					return sound;
				},

				/**
				* Automatically called periodically to help update visual user interface.
				*/
				'poll': function () {
					for (var soundurl in window.metaudio.sounds) {
						var sound = window.metaudio.sounds[soundurl];
						if (sound.playing) {
							sound.whileplaying();
						}
						if (sound.loading) {
							sound.whileloading();
							var total = sound.getTotal();
							var loaded = sound.getLoaded();
							if (total > 0 && loaded >= total) {
								sound.loading = false;
							}
						}
					}
				}
			},

			'create': function (attrs) {
				var self = this;

				/**
				* True if the audio format is supported by the browser via the HTML5 element <audio>.
				* @param {string} format The audio format MIME type.
				*/
				function isAudioFormatSupported(format) {
					return html5audio && ['probably','maybe'].contains(html5audio.canPlayType('audio/' + format));
				}

				var url = attrs.url;
				var player;
				var audiosupported = /\.ogg$/.test(url) && isAudioFormatSupported('ogg') || /\.(m4a|mp4)$/.test(url) && isAudioFormatSupported('mp4') || /\.mp3$/.test(url) && isAudioFormatSupported('mpeg');
				if (audiosupported && !settings.spectrogram && !settings.peak) {  // use Flash only if absolutely necessary
					player = self.html5player;
				} else if (Browser.Plugins.Flash.version && self.flashplayer.player && /\.(m4a|mp4|mp3)$/.test(url)) {  // visualization features require Flash
					player = self.flashplayer;
				} else {  // Flash not supported on platform, disable visualization
					player = self.html5player;
				}

				// add sound to collection
				var sound = player.create(attrs);
				self.sounds[attrs.url] = sound;
				return sound;
			},

			/**
			* Fired when a sound has been buffered and is ready to play.
			* This function is a callback, invoked from ActionScript.
			*/
			'onload': function (url) {
				var sound = this.sounds[url];
				sound.onload();
			},

			/**
			* Fired when a sound starts playing.
			* This function is a callback, invoked from ActionScript.
			*/
			'onplay': function (url) {
				var sound = this.sounds[url];
				sound.playing = true;
				sound.onplay();
			},

			/**
			* Fired when a sound stops playing.
			* This function is a callback, invoked from ActionScript.
			*/
			'onstop': function (url) {
				var sound = this.sounds[url];
				sound.playing = false;
				sound.onstop();
			},

			/**
			* Fired when a sound that has been paused resumes playing.
			* This function is a callback, invoked from ActionScript.
			*/
			'onresume': function (url) {
				var sound = this.sounds[url];
				sound.playing = true;
				sound.onresume();
			},

			/**
			* Fired when a sound that has started playing is paused.
			* This function is a callback, invoked from ActionScript.
			*/
			'onpause': function (url) {
				var sound = this.sounds[url];
				sound.playing = false;
				sound.onpause();
			},

			/**
			* Fired when a sound completes playing.
			* This function is a callback, invoked from ActionScript.
			*/
			'onfinish': function (url) {
				var sound = this.sounds[url];
				sound.playing = false;
				sound.onfinish();
			},

			/**
			* Fired when a new playhead position is set.
			* This function is a callback, invoked from ActionScript.
			* @param pos The new position set.
			*/
			'onseek': function (url, pos) {
				this.sounds[url].onseek(pos);
			},

			/**
			* Pauses playing all sounds except the one specified.
			*/
			'pauseAllBut': function (url) {
				for (var soundurl in this.sounds) {
					if (soundurl != url) {
						this.sounds[soundurl].pause();
					}
				}
			}
		};
	}
};
