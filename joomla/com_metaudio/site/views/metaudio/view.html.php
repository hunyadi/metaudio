<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );

/**
* HTML View class for the metaudio Component
*/
class metaudioViewMetAudio extends JViewLegacy {
	private function getResourceRelativePath($relpath) {
		$basename = pathinfo($relpath, PATHINFO_BASENAME);  // e.g. "sigplus.css"
		$folder = pathinfo($relpath, PATHINFO_DIRNAME);  // e.g. "/plugins/content/sigplus/css"
		$p = strrpos($basename, '.');  // search from backwards
		if ($p !== false) {
			$filename = substr($basename, 0, $p);  // drop extension from filename
			$extension = substr($basename, $p);
		} else {
			$filename = $basename;
			$extension = '';
		}

		$path = JPATH_ROOT.str_replace('/', DIRECTORY_SEPARATOR, $relpath);
		$dir = pathinfo($path, PATHINFO_DIRNAME);
		$original = $dir.DIRECTORY_SEPARATOR.$basename;
		$minified = $dir.DIRECTORY_SEPARATOR.$filename.'.min'.$extension;
		$app = JFactory::getApplication('site');
		$params = $app->getParams('com_metaudio');
		if (!$params->get('debug', false) && (!file_exists($original) || file_exists($minified) && filemtime($minified) >= filemtime($original))) {
			return $folder.'/'.$filename.'.min'.$extension;
		} else {
			return $relpath;
		}
	}

	/**
	* Returns the minified version of a style or script file if available.
	*/
	private function getResourceURL($relpath) {
		return JURI::base(true).$this->getResourceRelativePath($relpath);
	}

	/**
	* Authorizes access to the audio management interface.
	*/
	private function authorizeUser() {
		$user = JFactory::getUser();
		if ($user->guest) {  // only logged-in users are allowed to make edits
			return false;
		} elseif (!$user->authorise('core.edit', 'com_metaudio')) {
			return false;
		} else {
			return true;
		}
	}

	public function display($tpl = null) {
		$model = $this->getModel();

		$app = JFactory::getApplication('site');
		$menus = $app->getMenu();
		$menu = $menus->getActive();
		if (is_object($menu)) {
			$menuid = $menu->id;
			$title = $menu->title;
		} else {
			$title = 'metaudio';
		}

		// fetch component parameters with overrides
		$params = $app->getParams('com_metaudio');

		$folder = trim($params->get('folder'), '/');  // the web folder for audio recordings
		$sortorder = $params->get('sortorder', 'filename-asc');
		$image = $params->get('image', 0);
		$download = $params->get('download', 0);
		$feed = $params->get('feed', 0);
		$showedit = $this->authorizeUser();

		// create view and edit links
		$listing = $model->getListing($folder, $sortorder);
		$links = array();
		foreach ($listing as $file => $metadata) {
			$path = $folder.'/'.$file;
			if (isset($menuid)) {
				$viewlink = 'f='.$path.'&option=com_metaudio&controller=recording&Itemid='.$menuid;
				$editlink = 'f='.$path.'&option=com_metaudio&controller=recording&task=edit&Itemid='.$menuid;
			} else {
				$viewlink = 'f='.$path.'&option=com_metaudio&controller=recording';
				$editlink = 'f='.$path.'&option=com_metaudio&controller=recording&task=edit';
			}
			$links[$file] = array();
			if ($metadata['count'] > 0) {
				$links[$file]['view'] = JRoute::_('index.php?'.$viewlink);
			}

			if ($showedit && $model->isEditable($path)) {
				$links[$file]['edit'] = JRoute::_('index.php?'.$editlink);
			}
		}

		// assign variables available in template
		$this->assignRef('title', $title);
		$this->assignRef('folder', $folder);
		$this->assignRef('listing', $listing);
		$this->assignRef('links', $links);
		$this->assignRef('showimage', $image);
		$this->assignRef('showdownload', $download);
		$this->assignRef('showfeed', $feed);

		// add style sheet and script references
		$doc = JFactory::getDocument();
		switch ($params->get('player', 'simple')) {
			case 'deprecated':
				$doc->addStyleSheet($this->getResourceURL('/media/metaudio/css/inlineplayer.css'));
				if ($params->get('debug', false)) {
					$scripts = array('soundmanager2.js','inlineplayer.js','utility.js');
				} else {
					$scripts = array('soundmanager2-nodebug-jsmin.js','inlineplayer.min.js','utility.min.js');
				}
				$inlinescript = 'soundManager.url="'.JURI::base(true).'/media/metaudio/swf/";'."\n".
					'soundManager.onready(function() { '.
						'if(soundManager.supported()) { '.
							'inlinePlayer = new InlinePlayer(); inlinePlayer.config.playNext = '.( $params->get('autonext') ? 'true' : 'false' ).';'.
						' }'.
					' });';
				break;
			case 'advanced':
			case 'advanced-mootools':
				// add advanced player stylesheet
				$doc->addStyleSheet($this->getResourceURL('/media/metaudio/css/player.css'));

				// load MooTools, which is native to Joomla, modify Joomla if you wish to load it from a CDN
				JHTML::_('behavior.framework');

				// add canvas emulation for IE < 9
				if ($doc->getType() == 'html') {  // custom tags are supported by HTML document type only
					$doc->addCustomTag('<!--[if lt IE 9]><script type="text/javascript" src="'.JURI::base(true).'/media/metaudio/js/excanvas.js"></script><![endif]-->');
				}

				// add advanced player scripts
				if ($params->get('debug', false)) {
					$scripts = array('metaudio.mootools.js','ui-advanced.mootools.js','utility.js');
				} else {
					$scripts = array('metaudio.mootools.min.js','ui-advanced.mootools.min.js','utility.js');
				}

				// tell .js implementation where to look for .swf file
				$inlinescript =
					'metaudioPlayer('.
					json_encode(
						array(
							'swfurl' => JURI::base(true).'/media/metaudio/swf/metaudio.swf',
							'autoPlayNext' => (bool)$params->get('autonext'),
							'clock' => (bool)$params->get('clock'),
							'peak' => (bool)$params->get('peak'),
							'spectrogram' => (bool)$params->get('spectrogram'),
							'statusbar' => (bool)$params->get('statusbar')
						)
					).
					');'
				;
				break;
			case 'advanced-jquery':
				// add advanced player stylesheet
				$doc->addStyleSheet($this->getResourceURL('/media/metaudio/css/player.css'));

				// add dynamic Flash object creation
				$doc->addScript('http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js');

				// add canvas emulation for IE < 9
				if ($doc->getType() == 'html') {  // custom tags are supported by HTML document type only
					$doc->addCustomTag('<!--[if lt IE 9]><script type="text/javascript" src="'.JURI::base(true).'/media/metaudio/js/excanvas.js"></script><![endif]-->');
				}

				// add jQuery library
				JHTML::_('jquery.framework');

				// add advanced player scripts
				if ($params->get('debug', false)) {
					$scripts = array('metaudio.jquery.js','utility.js');
				} else {
					$scripts = array('metaudio.jquery.min.js','utility.min.js');
				}

				// tell .js implementation where to look for .swf file
				$inlinescript = 'metaudioPlayer({ '.
					'swfurl:"'.JURI::base(true).'/media/metaudio/swf/metaudio.swf", '.
					'autoPlayNext:'.( $params->get('autonext') ? 'true' : 'false' ).
				' });';
				break;
			case 'simple':
			case 'simple-mootools':
			default:
				// add simple player stylesheet
				$doc->addStyleSheet($this->getResourceURL('/media/metaudio/css/inlineplayer.css'));

				// load MooTools, which is native to Joomla, modify Joomla if you wish to load it from a CDN
				JHTML::_('behavior.framework');

				// add simple player scripts
				if ($params->get('debug', false)) {
					$scripts = array('metaudio.mootools.js','ui-simple.mootools.js','utility.js');
				} else {
					$scripts = array('metaudio.mootools.min.js','ui-simple.mootools.min.js','utility.js');
				}

				// tell .js implementation where to look for .swf file
				$inlinescript =
					'metaudioPlayer('.
					json_encode(
						array(
							'swfurl' => JURI::base(true).'/media/metaudio/swf/metaudio.swf',
							'autoPlayNext' => (bool)$params->get('autonext'),
							'clock' => (bool)$params->get('clock'),
							'peak' => false,
							'spectrogram' => false,
							'statusbar' => (bool)$params->get('statusbar')
						)
					).
					');'
				;
				break;
		}
		
		foreach ($scripts as $script) {
			$doc->addScript($this->getResourceURL('/media/metaudio/js/'.$script));
		}
		$doc->addScriptDeclaration($inlinescript);

		if ($feed) {
			// add references to RSS/Atom feeds to page <head> section with URLs wrapped in <link> elements
			$feedurl = 'index.php?option=com_metaudio&format=feed';
			$rss = array(
				'type' => 'application/rss+xml',
				'title' => 'RSS 2.0'
			);
			$atom = array(
				'type' => 'application/atom+xml',
				'title' => 'Atom'
			);
			$doc->addHeadLink(JRoute::_($feedurl.'&type=rss'), 'alternate', 'rel', $rss);
			$doc->addHeadLink(JRoute::_($feedurl.'&type=atom'), 'alternate', 'rel', $atom);
		}

		parent::display($tpl);
	}
}
