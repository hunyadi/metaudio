<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

require_once JPATH_COMPONENT.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'escape.php';
?>

<?php
if ($this->title) {
	print '<h1>'.$this->title.'</h1>';
}
?>
<?php
$app = JFactory::getApplication('site');
if (!empty($this->listing)) {
	print '<ul class="metaudio">';
	foreach ($this->listing as $file => $metadata) {
		$path = $this->folder.'/'.rawurlencode($file);
		$url = JURI::base(true).'/'.$path;
		print '<li>';

		// output item header
		print '<div class="metaudio-item" onclick="metaudio_accordion(this);">';

		// enable/disable accordion
		if (isset($metadata['description']) || isset($metadata['comment'])) {
			print '<span></span>';
		} else {
			print '<span class="missing"></span>';
		}

		// set audio file artist and/or title
		if (isset($metadata['artist']) && isset($metadata['title'])) {
			$anchortext = $metadata['artist'].': '.$metadata['title'];
		} elseif (isset($metadata['title'])) {
			$anchortext = $metadata['title'];
		} else {
			$anchortext = $file;
		}

		// show small image next to artist and/or title
		if ($this->showimage && isset($metadata['image'])) {
			$imagedata = $metadata['image'];
			$imagepath = $this->folder.'/'.rawurlencode($imagedata['file']);
			$imageurl = JURI::base(true).'/'.$imagepath;
			$imagewidth = $imagedata['width'];
			$imageheight = $imagedata['height'];
			$anchortext = '<img width="'.$imagewidth.'" height="'.$imageheight.'" src="'.$imageurl.'" /> '.$anchortext;
		}

		// prevent audio link from being picked up by robots
		$link = '<a class="metaudio-player" href="'.$url.'">'.$anchortext.'</a>';
		print '<script type="text/javascript"><!--'."\n".'document.write(metaudio_decode("'.str_rot13(base64_encode(str_rot13($link))).'")); //--></script>';
		print '<noscript>'.$anchortext.'</noscript>';

		// show download link
		if ($this->showdownload) {
			$absolutepath = realpath(JPATH_BASE.DIRECTORY_SEPARATOR.str_replace('/', DIRECTORY_SEPARATOR, $this->folder).DIRECTORY_SEPARATOR.$file);
			$downloadurl = JURI::base(true).'/components/com_metaudio/assets/download.php?f='.$path.'&h='.md5($absolutepath.$app->getCfg('secret'));  // use secret key to generate hashes more difficult to decypher
			print ' <a href="'.$downloadurl.'">'.JText::_('METAUDIO_RECORDING_DOWNLOAD').'</a>';
		}

		// show view metadata link
		if (isset($this->links[$file]['view'])) {
			print ' <a href="'.JRoute::_($this->links[$file]['view']).'">'.JText::_('METAUDIO_RECORDING_VIEW').'</a>';
		}

		// show edit metadata link
		if (isset($this->links[$file]['edit'])) {
			print ' <a href="'.JRoute::_($this->links[$file]['edit']).'">'.JText::_('METAUDIO_RECORDING_EDIT').'</a>';
		}
		print '</div>';

		// output item details
		print '<div class="metaudio-itemdetails">';
		if (isset($metadata['description']) && isset($metadata['comment'])) {
			$text = $metadata['description']."\n".$metadata['comment'];
		} elseif (isset($metadata['description'])) {
			$text = $metadata['description'];
		} elseif (isset($metadata['comment'])) {
			$text = $metadata['comment'];
		} else {
			$text = false;
		}
		print html_escape_and_link_urls($text);
		print '</div>';

		print '</li>';
	}
	print '</ul>';
	print '<div id="metaudio-placeholder"></div>';
} else {
	print JText::_('METAUDIO_EMPTY');
}

if ($this->showfeed) {
	// build RSS 2.0 feed and Atom feed URL and output HTML anchors
	$feedurl = 'index.php?option=com_metaudio&format=feed';
	print '<a href="'.JRoute::_($feedurl.'&type=rss').'">RSS</a> <a href="'.JRoute::_($feedurl.'&type=atom').'">Atom</a>';
}
