<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );

require_once JPATH_COMPONENT.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'escape.php';

/**
* HTML View class for the metaudio Component
*/
class metaudioViewMetAudio extends JViewLegacy {
	private function getResourceRelativePath($relpath) {
		$basename = pathinfo($relpath, PATHINFO_BASENAME);  // e.g. "sigplus.css"
		$folder = pathinfo($relpath, PATHINFO_DIRNAME);  // e.g. "/plugins/content/sigplus/css"
		$p = strrpos($basename, '.');  // search from backwards
		if ($p !== false) {
			$filename = substr($basename, 0, $p);  // drop extension from filename
			$extension = substr($basename, $p);
		} else {
			$filename = $basename;
			$extension = '';
		}

		$path = JPATH_ROOT.str_replace('/', DIRECTORY_SEPARATOR, $relpath);
		$dir = pathinfo($path, PATHINFO_DIRNAME);
		$original = $dir.DIRECTORY_SEPARATOR.$basename;
		$minified = $dir.DIRECTORY_SEPARATOR.$filename.'.min'.$extension;
		$app = JFactory::getApplication('site');
		$params = $app->getParams('com_metaudio');
		if (!$params->get('debug', false) && (!file_exists($original) || file_exists($minified) && filemtime($minified) >= filemtime($original))) {
			return $folder.'/'.$filename.'.min'.$extension;
		} else {
			return $relpath;
		}
	}

	/**
	* Returns the minified version of a style or script file if available.
	*/
	private function getResourceURL($relpath) {
		return JURI::base(true).$this->getResourceRelativePath($relpath);
	}

	/**
	* Authorizes access to the audio management interface.
	*/
	private function authorizeUser() {
		$user = JFactory::getUser();
		if ($user->guest) {  // only logged-in users are allowed to make edits
			return false;
		} elseif (!$user->authorise('core.edit', 'com_metaudio')) {
			return false;
		} else {
			return true;
		}
	}

	public function display($tpl = null) {
		$model = $this->getModel();

		$app = JFactory::getApplication('site');
		$menus = $app->getMenu();
		$menu = $menus->getActive();
		if (is_object($menu)) {
			$menuid = $menu->id;
			$title = $menu->title;
		} else {
			$title = 'metaudio';
		}

		// fetch component parameters with overrides
		$params = $app->getParams('com_metaudio');

		$folder = trim($params->get('folder'), '/');  // the web folder for audio recordings
		$basepath = JPATH_BASE.DIRECTORY_SEPARATOR.str_replace('/', DIRECTORY_SEPARATOR, $folder);
		$sortorder = $params->get('sortorder', 'filename-asc');

		// get main page link and recording view links
		if (isset($menuid)) {
			$pagelink = 'option=com_metaudio&Itemid='.$menuid;
		} else {
			$pagelink = 'option=com_metaudio';
		}

		$listing = $model->getListing($folder, $sortorder);
		$links = array();
		foreach ($listing as $file => $metadata) {
			$path = $folder.'/'.$file;
			if (isset($menuid)) {
				$viewlink = 'f='.$path.'&option=com_metaudio&controller=recording&Itemid='.$menuid;
				$editlink = 'f='.$path.'&option=com_metaudio&controller=recording&task=edit&Itemid='.$menuid;
			} else {
				$viewlink = 'f='.$path.'&option=com_metaudio&controller=recording';
				$editlink = 'f='.$path.'&option=com_metaudio&controller=recording&task=edit';
			}
			$links[$file] = array();
			if ($metadata['count'] > 0) {
				$links[$file]['view'] = JRoute::_('index.php?'.$viewlink);
			}
		}

		// output RSS/Atom feed
		$document = JFactory::getDocument();
		$document->setTitle($title);
		$document->setLink(JRoute::_('index.php?'.$pagelink));

		// output RSS/Atom feed items
		if (!empty($listing)) {
			foreach ($listing as $file => $metadata) {
				// recording title
				if (isset($metadata['artist']) && isset($metadata['title'])) {
					$itemtitle = $metadata['artist'].': '.$metadata['title'];
				} elseif (isset($metadata['title'])) {
					$itemtitle = $metadata['title'];
				} else {
					$itemtitle = $file;
				}

				// recording description
				if (isset($metadata['description']) && isset($metadata['comment'])) {
					$itemdescription = $metadata['description']."\n".$metadata['comment'];
				} elseif (isset($metadata['description'])) {
					$itemdescription = $metadata['description'];
				} elseif (isset($metadata['comment'])) {
					$itemdescription = $metadata['comment'];
				} else {
					$itemdescription = false;
				}
				if ($itemdescription !== false) {
					$itemdescription = html_escape_and_link_urls($itemdescription);
				}

				// recording last modified date
				$absolutepath = realpath($basepath.DIRECTORY_SEPARATOR.$file);
				if (file_exists($absolutepath)) {
					$itemdate = filemtime($absolutepath);
				} else {
					$itemdate = time();
				}

				// create feed item
				$item = new JFeedItem();
				$item->title = $itemtitle;
				$item->link = $links[$file]['view'];
				$item->date = $itemdate;
				if ($itemdescription !== false) {
					$item->description = $itemdescription;
				}
				$document->addItem($item);
			}
		}

		// redirect to Joomla for raw RSS/Atom output
		parent::display($tpl);
	}
}
