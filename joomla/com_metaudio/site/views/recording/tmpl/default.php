<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

require_once JPATH_COMPONENT.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'escape.php'; 
?>
<table>
<?php

if ($this->showplay) {
	$url = JURI::base(true).'/'.$this->file;

	// output item header
	print '<ul class="metaudio"><li><div class="metaudio-item">';

	// set audio file artist and/or title
	if (isset($this->metastrings['Artist']) && strlen($this->metastrings['Artist']) > 0 && isset($this->metastrings['Title']) && strlen($this->metastrings['Title']) > 0) {
		$anchortext = $this->metastrings['Artist'].': '.$this->metastrings['Title'];
	} elseif (isset($this->metastrings['Title']) && strlen($this->metastrings['Title']) > 0) {
		$anchortext = $this->metastrings['Title'];
	} else {
		$anchortext = basename($this->file);
	}

	// prevent audio link from being picked up by robots
	$link = '<a class="metaudio-player" href="'.$url.'">'.$anchortext.'</a>';
	print '<script type="text/javascript"><!--'."\n".'document.write(metaudio_decode("'.str_rot13(base64_encode(str_rot13($link))).'")); //--></script>';
	print '<noscript>'.$anchortext.'</noscript>';

	print '</div></li></ul>';
	print '<div id="metaudio-placeholder"></div>';
}

if ($this->showpath) {
	print '<tr><th>'.metaudioViewRecording::_('FILENAME').'</th><td>';
	print '<a href="'.JURI::base(true).'/'.$this->file.'">'.$this->file.'</a>';
	if (isset($this->editlink)) {
		print ' <a href="'.$this->editlink.'">'.JText::_('METAUDIO_RECORDING_EDIT').'</a>';
	}
	print '</td></tr>';
}

if (isset($this->metastrings)) {
	foreach ($this->metastrings as $key => $value) {
		if ($value !== false) {
			print '<tr><th>'.metaudioViewRecording::_($key).'</th><td>';
			switch ($key) {
				case 'Genre':
					if (preg_match('#^[(](\d+)[)](.*)$#Su', $value, $matches)) {  // check for genre text that begins with a genre code
						print $matches[2];  // print genre text only
					} else {
						print $value;
					}
					break;
				case 'Genre Code':
					if (preg_match('#^[(](\d+)[)](.*)$#Su', $value, $matches)) {  // check for genre code that includes a genre name
						print $matches[1];  // print genre code only
					} else {
						print $value;
					}
					break;
				case 'Cover':
					print $value;  // no HTML escape for cover image
					break;
				default:
					switch ($value) {
						case 'Yes':
						case 'No':
							print JText::_('METAUDIO_'.strtoupper($value));
							break;
						default:
							print html_escape_and_link_urls($value);
					}
			}
			print '</td></tr>';
		} else {
			//print '<tr><th>'.metaudioViewRecording::_($key).'</th><td>'.JText::_('METAUDIO_MISSING').'</td></tr>';
		}
	}
}

?>
</table>
<?php

if (isset($this->container)) {
	print '<h2>'.JText::_('METAUDIO_STRUCTURE').'</h2>';
	print '<pre>';
	print $this->container;
	print '</pre>';
}

?>