<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

function metaudio_control_buttons() {
?>
<button type="submit" name="task" value="save"><?php print JText::_('METAUDIO_SAVE'); ?></button>
<button type="submit" name="task" value="apply"><?php print JText::_('METAUDIO_APPLY'); ?></button>
<button type="reset"><?php print JText::_('METAUDIO_RESET'); ?></button>
<button type="submit" name="task" value="cancel"><?php print JText::_('METAUDIO_CANCEL'); ?></button>
<?php
}

if (isset($this->metadata)) {

print '<form enctype="multipart/form-data" method="post">';
metaudio_control_buttons();
print '<table>';
print '<tr><th>'.metaudioViewRecording::_('Filename').'</th><td>'.$this->file.'</td></tr>';
print '<tr><th>'.JText::_('METAUDIO_SAVE_AS').'</th><td><input name="filename" type="text" value="'.$this->file.'" size="60" /></td></tr>';

$textboxfield = array(
	'Title',
	'Album',
	'Artist',
	'Album Artist',
	'Year',
	'Genre',
	'Composer',
	'Encoder',
	'Copyright',
	'Grouping',
	'Category',
	'Keywords',
	'Copyright URL',
	'Audio Webpage',
	'Artist Webpage',
	'Audio Source Webpage',
	'Publisher Webpage',
	'TV Network Name',
	'TV Show Name');
$textareafield = array(
	'Comment',
	'Description',
	'Lyrics');
$numericfield = array(
	'Tempo',
	'TV Season',
	'TV Episode');
$checkboxfield = array(
	'Compilation',
	'Gapless Playback');

foreach ($this->metadata as $key => $value) {
	print '<tr><th>'.metaudioViewRecording::_($key).'</th><td>';
	if (in_array($key, $textboxfield)) {  // text field
		print '<input name="meta['.$key.']" type="text" size="60" value="'.$value.'" />';
	} elseif (in_array($key, $textareafield)) {  // multi-line text field
		print '<textarea name="meta['.$key.']" cols="60" rows="4">'.$value.'</textarea>';
	} elseif (in_array($key, $numericfield)) {
		print '<input name="meta['.$key.']" type="text" size="4" value="'.$value.'" />';	
	} elseif (in_array($key, $checkboxfield)) {
		print '<input name="meta['.$key.']" type="radio" value="1"'.($value ? 'checked="checked"' : '').' /> '.JText::_('METAUDIO_YES').' <input name="meta['.$key.']" type="radio" value="0"'.(!$value ? 'checked="checked"' : '').' /> '.JText::_('METAUDIO_NO');
	} else {
		switch ($key) {
			case 'File Type':
				print $value['type'] . (empty($value['compatible']) ? '' : ' ('.implode(', ', $value['compatible'])).')';
				break;
			case 'Recording Copyright':
				require_once JPATH_COMPONENT.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'lang.php';
				print '<select name="meta['.$key.'][lang]"><option value="">'.JText::_('METAUDIO_UNSPECIFIED').'</option>';
				foreach ($languages as $langcode => $langname) {
					print '<option value="'.$langcode.'"'.($langcode === $value['lang'] ? ' selected="selected"' : '').'>'.$langname.'</option>';
				}
				print '</select><br/><input name="meta['.$key.'][text]" type="text" size="40" value="'.$value['text'].'" />';
				break;
			case 'Genre Code':
				print '<select name="meta['.$key.']">';
				print '<option value="">'.JText::_('METAUDIO_MISSING').'</option>';
				$genres = metaudioViewRecording::getGenres();
				asort($genres);
				foreach ($genres as $key => $genre) {
					print '<option value="'.$key.'"'.(isset($value['code']) && $key === $value['code'] ? ' selected="selected"' : '').'>'.$genre.'</option>';
				}
				print '</select>';
				break;
			case 'Date':
				if (is_array($value)) {
					$text = $value['month'].'-'.$value['day'];
				} else {
					$text = $value;
				}
				print '<input name="meta['.$key.']" type="text" size="60" value="'.$text.'" />';
				break;
			case 'Time':
				if (is_array($value)) {
					$text = $value['hour'].':'.$value['minute'];
				} else {
					$text = $value;
				}
				print '<input name="meta['.$key.']" type="text" size="60" value="'.$text.'" />';
				break;
			case 'Track':
			case 'Disk':
				if (empty($value)) {
					$value['number'] = false;
					$value['total'] = false;
				}
				print '<input name="meta['.$key.'][number]" type="text" size="4" value="'.$value['number'].'" /> '.JText::_('METAUDIO_OF').' <input name="meta['.$key.'][total]" type="text" size="4" value="'.$value['total'].'" />';
				break;
			case 'Cover':
				if ($value) {
					print '<img src="'.'data:' . $value['mime'] . ';base64,' . base64_encode($value['data']) .'" /><br />';
				}
				print '<input name="meta['.$key.']" type="checkbox" value="" /> '.JText::_('METAUDIO_REMOVE_EXISTING').'<br />';  // force cover art removal
				print '<input name="meta-'.$key.'" type="file" />';
				break;
			default:
				print nl2br($value);
		}
	}
	print '</td></tr>';
}

print '</table>';
metaudio_control_buttons();

?>

<input type="hidden" name="option" value="com_metaudio" />
<input type="hidden" name="controller" value="recording" />
</form>

<form method="post">
<label for="encoding"><?php print JText::_('METAUDIO_ENCODING'); ?></label><br/>
<select onchange="if(this.value){this.form.elements['encoding'].value = this.value;}">
<option value=""><?php print JText::_('METAUDIO_UNSPECIFIED'); ?></option>
<?php
	require_once JPATH_COMPONENT.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'codepage.php';
	foreach ($codepages as $key => $name) {
		print '<option value="'.$key.'">'.$name.'</option>';
	}
?>
</select>
<input name="encoding" type="text" size="12" />
<button type="submit" name="task" value="edit"><?php print JText::_('METAUDIO_RELOAD'); ?></button>
<input type="hidden" name="f" value="<?php print $this->file; ?>" />
<input type="hidden" name="option" value="com_metaudio" />
<input type="hidden" name="controller" value="recording" />
</form>

<?php
} else {
print '<table>';
print '<tr><th>'.metaudioViewRecording::_('Filename').'</th><td>'.$this->file.'</td></tr>';
print '<tr><th>'.JText::_('METAUDIO_DETAILS').'</th><td>'.JText::_('METAUDIO_UNSUPPORTED').'</td></tr>';
print '</table>';
}