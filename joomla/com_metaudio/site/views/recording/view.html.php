<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
* HTML View class for the metaudio Component
*/
class metaudioViewRecording extends JViewLegacy {
	private function getResourceRelativePath($relpath) {
		$basename = pathinfo($relpath, PATHINFO_BASENAME);  // e.g. "sigplus.css"
		$folder = pathinfo($relpath, PATHINFO_DIRNAME);  // e.g. "/plugins/content/sigplus/css"
		$p = strrpos($basename, '.');  // search from backwards
		if ($p !== false) {
			$filename = substr($basename, 0, $p);  // drop extension from filename
			$extension = substr($basename, $p);
		} else {
			$filename = $basename;
			$extension = '';
		}

		$path = JPATH_ROOT.str_replace('/', DIRECTORY_SEPARATOR, $relpath);
		$dir = pathinfo($path, PATHINFO_DIRNAME);
		$original = $dir.DIRECTORY_SEPARATOR.$basename;
		$minified = $dir.DIRECTORY_SEPARATOR.$filename.'.min'.$extension;
		$app = JFactory::getApplication('site');
		$params = $app->getParams('com_metaudio');
		if (!$params->get('debug', false) && (!file_exists($original) || file_exists($minified) && filemtime($minified) >= filemtime($original))) {
			return $folder.'/'.$filename.'.min'.$extension;
		} else {
			return $relpath;
		}
	}

	/**
	* Returns the minified version of a style or script file if available.
	*/
	private function getResourceURL($relpath) {
		return JURI::base(true).$this->getResourceRelativePath($relpath);
	}

	public static function _($key) {
		$keyname = 'METAUDIO_META_'.str_replace(' ', '_', strtoupper($key));
		$keystring = JText::_($keyname);
		return $keyname != $keystring ? $keystring : $key;
	}

	public static function getGenres() {
		return metadata_core::$genres;
	}

	/**
	* Authorizes access to the audio management interface.
	*/
	private function authorizeUser() {
		$user = JFactory::getUser();
		if ($user->guest) {  // only logged-in users are allowed to make edits
			return false;
		} else if (!$user->authorise('core.edit', 'com_metaudio')) {
			return false;
		} else {
			return true;
		}
	}

	public function display($tpl = null) {
		$model = $this->getModel();

		// fetch component parameters with overrides
		$params = JComponentHelper::getParams('com_metaudio');
		$showcontainer = $params->get('container', 0);

		$file = JRequest::getVar('f');
		$this->assignRef('file', $file);

		$showplay = true;
		if ($showplay) {
			$doc = JFactory::getDocument();

			// add advanced player stylesheet
			$doc->addStyleSheet($this->getResourceURL('/media/metaudio/css/player.css'));

			// add canvas emulation for IE < 9
			if ($doc->getType() == 'html') {  // custom tags are supported by HTML document type only
				$doc->addCustomTag('<!--[if lt IE 9]><script type="text/javascript" src="'.JURI::base(true).'/media/metaudio/js/excanvas.js"></script><![endif]-->');
			}

			// add advanced player scripts
			JHTML::_('behavior.framework');
			if ($params->get('debug', false)) {
				$scripts = array('metaudio.mootools.js','ui-advanced.mootools.js','utility.js');
			} else {
				$scripts = array('metaudio.mootools.min.js','ui-advanced.mootools.min.js','utility.min.js');
			}

			// tell .js implementation where to look for .swf file
			$inlinescript = 'metaudioPlayer({ '.
				'swfurl:"'.JURI::base(true).'/media/metaudio/swf/metaudio.swf", '.
				'autoPlayNext:'.( $params->get('autonext') ? 'true' : 'false' ).
			' });';

			foreach ($scripts as $script) {
				$doc->addScript($this->getResourceURL('/media/metaudio/js/'.$script));
			}
			$doc->addScriptDeclaration( $inlinescript );
		}
		$this->assignRef('showplay', $showplay);

		if (!$this->authorizeUser()) {  // use database to load metadata
			$showpath = false;
			$metastrings = $model->queryMetadata($file);

			$this->assignRef('metastrings', $metastrings);
		} else {  // use file to load metadata
			$showpath = true;
			$metastrings = $model->loadMetadata($file, $metadata, $container);
			if ($metastrings) {
				// make an attempt at correcting the character encoding
				$encoding = JRequest::getVar('encoding');
				if ($encoding) {
					foreach ($metadata as $key => &$value) {
						if (is_string($value)) {
							$text = iconv('utf-8', 'iso-8859-1', $value);  // convert back into single-byte character encoding
							$len = @iconv_strlen($text, $encoding);  // detect illegal characters in input string
							if ($len !== false) {
								$value = iconv($encoding, 'utf-8', $text);  // convert into UTF-8 with assumed true encoding
							}
						}
					}
				}
				$this->assignRef('metadata', $metadata);
				$this->assignRef('metastrings', $metastrings);
				if ($showcontainer) {
					$this->assignRef('container', $container);
				}
			}

			$app = JFactory::getApplication('site');
			$menu = $app->getMenu();
			$activemenu = $menu->getActive();
			if ($activemenu) {
				$menuid = $activemenu->id;
			}
			if ($model->isEditable($file)) {
				$url = 'index.php?f='.$file.'&option=com_metaudio&controller=recording&task=edit';
				if (isset($menuid)) {
					$url .= '&Itemid='.$menuid;
				}
				$editlink = JRoute::_($url);
				$this->assignRef('editlink', $editlink);
			}
		}
		$this->assignRef('showpath', $showpath);

		parent::display($tpl);
	}
}
