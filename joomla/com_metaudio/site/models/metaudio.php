<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'helper.php';
require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'filesystem.php';

jimport( 'joomla.application.component.model' );
jimport( 'joomla.language.latin_transliterate' );

/**
* metaudio Model.
*/
class metaudioModelMetAudio extends JModelLegacy {
	/**
	* The most important metadata for a file.
	*/
	private static function getFilesMetadata($folderpath) {
		$db = JFactory::getDBO();
		$db->setQuery(
			'SELECT '.
				'f.filename, '.
				'(SELECT textvalue FROM `#__metaudio_data` AS d INNER JOIN `#__metaudio_property` AS p ON d.propertyid = p.propertyid WHERE p.propertyname = \'Title\' AND d.fileid = f.fileid) AS `title`, '.
				'(SELECT textvalue FROM `#__metaudio_data` AS d INNER JOIN `#__metaudio_property` AS p ON d.propertyid = p.propertyid WHERE p.propertyname = \'Artist\' AND d.fileid = f.fileid) AS `artist`, '.
				'(SELECT textvalue FROM `#__metaudio_data` AS d INNER JOIN `#__metaudio_property` AS p ON d.propertyid = p.propertyid WHERE p.propertyname = \'Comment\' AND d.fileid = f.fileid) AS `comment`, '.
				'(SELECT textvalue FROM `#__metaudio_data` AS d INNER JOIN `#__metaudio_property` AS p ON d.propertyid = p.propertyid WHERE p.propertyname = \'Description\' AND d.fileid = f.fileid) AS `description`, '.
				'(SELECT COUNT(*) FROM `#__metaudio_data` AS d WHERE d.fileid = f.fileid) AS `count` '.
			'FROM `#__metaudio_folder` AS g '.
			'INNER JOIN `#__metaudio_file` AS f ON g.folderid = f.folderid '.
			'WHERE g.folderpath = '.$db->quote($folderpath)
		);
		return $db->loadAssocList('filename');
	}

	public function isEditable($filepath) {
		return metadata_core::is_editable($filepath);
	}

	/**
	* A list of audio files in a media folder.
	* @param folderpath A relative folder path w.r.t. the Joomla root.
	*/
	public function getListing($folderpath, $sortorder = 'filename-asc') {
		$folderabsolutepath = metaudioHelper::getPath($folderpath);
		$extensions = array('m4a','mp3','mp4','ogg');
		switch ($sortorder) {
			case 'playlist':
				$files = array();
				$playlistfiles = fsx::get_files_with_extension($folderabsolutepath, 'm3u');
				if (!empty($playlistfiles)) {
					foreach ($playlistfiles as $playlistfile) {
						$playlist = file($folderabsolutepath.DIRECTORY_SEPARATOR.$playlistfile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
						foreach ($playlist as $playlistitem) {
							if ($playlistitem[0] != '#' && file_exists($folderabsolutepath.DIRECTORY_SEPARATOR.$playlistitem)) {  // skip comments and missing files
								$files[] = $playlistitem;
							}
						}
					}
				} else {  // fallback to file name order
					$files = fsx::get_files_with_extension($folderabsolutepath, $extensions);
				}
				break;
			case 'filetime-desc':
			case 'filemtime-desc':
				$files = fsx::get_files_with_extension_time($folderabsolutepath, $extensions, true);
				break;
			case 'filetime-asc':
			case 'filemtime-asc':
				$files = fsx::get_files_with_extension_time($folderabsolutepath, $extensions);
				break;
			case 'filename-desc':
				$files = fsx::get_files_with_extension($folderabsolutepath, $extensions, true);
				break;
			case 'filename-asc':
			default:
				$files = fsx::get_files_with_extension($folderabsolutepath, $extensions);
		}

		metaudioHelper::scan($folderpath, $files);

		$metadata = self::getFilesMetadata($folderpath);
		$listing = array();
		foreach ($files as $file) {
			$listing[$file] = isset($metadata[$file]) ? $metadata[$file] : array();
			$filedata =& $listing[$file];

			// get icon for audio recording, matching audio file name
			if (!isset($filedata['image'])) {
				$filename = pathinfo($folderabsolutepath.DIRECTORY_SEPARATOR.$file, PATHINFO_FILENAME);
				$filedata['image'] = $this->getImageProperties($folderabsolutepath, $filename);
			}

			// get icon for audio recording, matching artist name
			if (!isset($filedata['image']) && isset($filedata['artist'])) {
				if (class_exists('JLanguageTransliterate')) {
					$artist = JLanguageTransliterate::utf8_latin_to_ascii($filedata['artist']);
				} else {
					$artist = $filedata['artist'];
				}
				$filedata['image'] = $this->getImageProperties($folderabsolutepath, $artist);
			}
		}
		unset($filedata);

		return $listing;
	}

	private function getImageProperties($folderabsolutepath, $filename) {
		foreach (array('.gif', '.jpeg', '.jpg', '.png') as $extension) {
			$imagefile = $filename.$extension;
			$imageabsolutepath = $folderabsolutepath.DIRECTORY_SEPARATOR.$imagefile;
			if (fsx::file_exists($imageabsolutepath) && ($imagesize = fsx::getimagesize($imageabsolutepath)) !== false) {
				list($imagewidth, $imageheight) = $imagesize;
				return array(
					'file' => $imagefile,
					'width' => $imagewidth,
					'height' => $imageheight
				);
			}
		}
		return null;
	}
}
