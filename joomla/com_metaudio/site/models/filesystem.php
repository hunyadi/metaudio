<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

/*****************************************************************************
* UTF-8 file system compatibility layer
*****************************************************************************/

if (!interface_exists('fsx_functions') && !class_exists('fsx')) {

/**
* UTF-8 compatibility layer for filesystem functions.
*
* This layer exposes PHP 5 filesystem functions like file_exists, filesize, filemtime, readfile,
* etc. that support files whose filename contains UTF-8 (non-ASCII) characters. Prefix function
* names with the static class name "fsx" to unlock functionality. For instance, the statement
*
* $size = fsx::filesize($file);
*
* returns the file size in bytes even if the variable $file contains non-ASCII characters.
*
* Normally, PHP 4/5 filesystem functions use ANSI versions of Windows filesystem functions, which
* fail with non-ASCII characters in filenames. This thin layer uses COM scripting services to
* interact with the Windows filesystem in a way that makes it possible to pass UTF-8 characters
* in filenames.
*/
interface fsx_functions {
	public function scandir($dir);
	public function scandir_files($dir);
	public function scandir_folders($dir);
	public function scandir_mtime($dir);
	public function get_files_with_extension($dir, array $ext, $rev = false);
	public function file_exists($path);
	public function filemtime($file);
	public function filesize($file);
	public function file_get_contents($file);
	public function file_put_contents($file, $data);
	public function readfile($file);
}

class fsx_windows implements fsx_functions {
	/**
	* A COM file system object in Windows.
	*/
	private $fs = null;

	public function __construct($fs) {
		$this->fs = $fs;
	}

	public function __destruct() {
		unset($this->fs);
	}

	private static function is_ascii($string) {
		return !preg_match('/[\\x80-\\xff]+/', $string);
	}

	public function get_short_path($path) {
		try {
			if ($this->fs->FileExists($path)) {
				return $this->fs->GetFile($path)->ShortPath;
			}
			if ($this->fs->FolderExists($path)) {
				return $this->fs->GetFolder($path)->ShortPath;
			}
		} catch (com_exception $e) {}
		return $path;  // no short path available
	}

	public function scandir($dir) {
		$results = array();
		try {
			$fsdir = $this->fs->GetFolder($dir);
			foreach ($fsdir->SubFolders as $fsfolder) {
				$results[] = $fsfolder->Name;
			}
			foreach ($fsdir->Files as $fsfile) {
				$results[] = $fsfile->Name;
			}
			unset($fsdir);
			return $results;
		} catch (com_exception $e) { }
		return false;
	}

	public function scandir_files($dir) {
		$results = array();
		try {
			$fsdir = $this->fs->GetFolder($dir);
			foreach ($fsdir->Files as $fsfile) {
				$results[] = $fsfile->Name;
			}
			unset($fsdir);
			return $results;
		} catch (com_exception $e) { }
		return false;
	}

	public function scandir_folders($dir) {
		$results = array();
		try {
			$fsdir = $this->fs->GetFolder($dir);
			foreach ($fsdir->SubFolders as $fsfolder) {
				$results[] = $fsfolder->Name;
			}
			unset($fsdir);
			return $results;
		} catch (com_exception $e) { }
		return false;
	}

	public function scandir_mtime($dir) {
		$results = array();
		try {
			$fsdir = $this->fs->GetFolder($dir);
			foreach ($fsdir->Files as $fsfile) {
				$results[$fsfile->Name] = $fsfile->DateLastModified;
			}
			unset($fsdir);
			return $results;
		} catch (com_exception $e) { }
		return false;
	}

	/**
	* Get a directory listing in character set UTF-8.
	*/
	public function get_files_with_extension($dir, array $ext, $rev = false) {
		$results = array();
		try {
			$fsdir = $this->fs->getFolder($dir);
			foreach ($fsdir->Files as $fsfile) {
				$fsname = $fsfile->Name;
				$fsext = '';
				if (($pos = strrpos($fsname, '.')) !== false) {
					$fsext = substr($fsname, $pos + 1);  // skip dot (.)
				}
				if (in_array($fsext, $ext)) {
					$results[] = $fsname;
				}
			}
			unset($fsdir);
			return $results;
		} catch (com_exception $e) { }
		return false;
	}

	public function file_exists($path) {
		return $this->fs->FileExists($path) || $this->fs->FolderExists($path);
	}

	public function filemtime($file) {
		try {
			if ($this->fs->FileExists($file)) {
				return variant_date_to_timestamp($this->fs->GetFile($file)->DateLastModified);
			}
			if ($this->fs->FolderExists($file)) {
				return variant_date_to_timestamp($this->fs->GetFolder($file)->DateLastModified);
			}
		} catch (com_exception $e) {}
		return false;
	}

	public function filesize($file) {
		try {
			if ($this->fs->FileExists($file)) {
				return $this->fs->GetFile($file)->Size;
			}
		} catch (com_exception $e) {}
		return false;
	}

	public function file_get_contents($file) {
		if (self::is_ascii($file)) {
			return file_get_contents($file);
		}

		$is_output_buffered = false;
		try {
			$stream = new COM('ADODB.Stream', null, CP_UTF8);  // use UTF-8 code page for communicating with COM object
			$stream->Mode = 3;  // adModeReadWrite
			$stream->Type = 1;  // adTypeBinary
			$stream->Open();
			$stream->LoadFromFile($file);  // read file into Stream object
			$data = $stream->Read();  // convert to PHP Traversable
			$is_output_buffered = ob_start();
			foreach ($data as $item) {  // iterate COM SAFEARRAY as character codes and print each character
				print chr($item);
			}
			$string = ob_get_clean();
			$stream->Close();
			return $string;
		} catch (com_exception $e) {
			if ($is_output_buffered) {
				ob_end_clean();
			}
			return file_get_contents($this->get_short_path($file));
		}
	}

	public function file_put_contents($file, $data) {
		try {
			$tempfile = tempnam(dirname($file), 'fsx');
			$result = file_put_contents($tempfile, $data);
			if ($this->fs->FileExists($file)) {
				$this->fs->DeleteFile($file);
			}
			$this->fs->MoveFile($tempfile, $file);
			return $result;
		} catch (com_exception $e) {
			if (isset($tempfile) && file_exists($tempfile)) {
				unlink($tempfile);
			}
			return false;
		}
	}

	public function readfile($file) {
		if (self::is_ascii($file)) {
			return readfile($file);
		}

		try {
			$stream = new COM('ADODB.Stream', null, CP_UTF8);  // use UTF-8 code page for communicating with COM object
			$stream->Mode = 3;  // adModeReadWrite
			$stream->Type = 1;  // binary
			$stream->Open();
			$stream->LoadFromFile($file);  // read file into Stream object
			$data = $stream->Read();  // convert to PHP Traversable
			foreach ($data as $item) {  // iterate COM SAFEARRAY as character codes and print each character
				print chr($item);
			}
			$stream->Close();
			return true;
		} catch (com_exception $e) {
			return readfile($this->get_short_path($file));
		}
	}

	public function getimagesize($file, &$info = null) {
		if (function_exists('getimagesizefromstring')) {
			return getimagesizefromstring($this->file_get_contents($file), $info);
		} else {
			return getimagesize($this->get_short_path($file), $info);
		}
	}

	public function imagecreatefromjpeg($file) {
		$content = $this->file_get_contents($file);
		return @imagecreatefromstring($content);
	}

	public function imagecreatefromgif($file) {
		$content = $this->file_get_contents($file);
		return @imagecreatefromstring($content);
	}

	public function imagecreatefrompng($file) {
		$content = $this->file_get_contents($file);
		return @imagecreatefromstring($content);
	}
	
	public function imagejpeg($image, $file, $quality) {
		ob_start();
		imagejpeg($image, null, $quality);
		$data = ob_get_clean();
		if ($data !== false) {
			$this->file_put_contents($file, $data);
			return true;
		} else {
			return false;
		}
	}
	
	public function imagegif($image, $file) {
		ob_start();
		imagegif($image);
		$data = ob_get_clean();
		if ($data !== false) {
			$this->file_put_contents($file, $data);
			return true;
		} else {
			return false;
		}
	}

	public function imagepng($image, $file, $quality) {
		ob_start();
		imagepng($image, null, $quality);
		$data = ob_get_clean();
		if ($data !== false) {
			$this->file_put_contents($file, $data);
			return true;
		} else {
			return false;
		}
	}
}

class fsx_unix implements fsx_functions {
	public function scandir($dir) {
		return scandir($dir);
	}

	public function scandir_files($dir) {
		$entries = scandir($dir);
		if ($entries !== false) {
			$results = array();
			foreach ($entries as $entry) {
				if (is_file($dir.DIRECTORY_SEPARATOR.$entry)) {
					$results[] = $entry;
				}
			}
			return $results;
		} else {
			return false;
		}
	}

	public function scandir_folders($dir) {
		$entries = scandir($dir);
		if ($entries !== false) {
			$results = array();
			foreach ($entries as $entry) {
				if (is_dir($dir.DIRECTORY_SEPARATOR.$entry)) {
					$results[] = $entry;
				}
			}
			return $results;
		} else {
			return false;
		}
	}

	/**
	* List files and directories inside the specified path with modification time.
	* @return An associative array with filenames as keys and timestamps as values.
	*/
	public function scandir_mtime($dir) {
		$dh = @opendir($dir);
		if ($dh === false) {  // cannot open directory
			return false;
		}
		$files = array();
		while (false !== ($filename = readdir($dh))) {
			if (!is_regular_file($filename)) {
				continue;
			}
			$files[$filename] = filemtime($dir.DIRECTORY_SEPARATOR.$filename);
		}
		closedir($dh);
		return $files;
	}

	/**
	* Get all files in a directory that have the specified extension.
	* @param {string} $dir Absolute path to a directory.
	* @param {string} $ext Extension to check file names against.
	* @param {boolean} $rev True if file names are to be sorted in reverse order.
	*/
	public function get_files_with_extension($dir, array $ext, $rev = false) {
		$results = array();
		$files = scandir($dir, $rev);
		foreach ($files as $file) {
			if (in_array(pathinfo($file, PATHINFO_EXTENSION), $ext)) {
				$results[] = $file;
			}
		}
		return $results;
	}

	public function file_exists($path) {
		return file_exists($path);
	}

	public function filemtime($file) {
		return filemtime($file);
	}

	public function filesize($file) {
		return filesize($file);
	}

	public function file_get_contents($file) {
		return file_get_contents($file);
	}

	public function file_put_contents($file, $data) {
		return file_put_contents($file, $data);
	}

	public function readfile($file) {
		return readfile($file);
	}

	public function getimagesize($file, &$info = null) {
		return getimagesize($file, $info);  // no special wrapper is required for UNIX-style operating systems
	}

	public function imagecreatefromjpeg($file) {
		return imagecreatefromjpeg($file);  // no special wrapper is required for UNIX-style operating systems
	}

	public function imagecreatefromgif($file) {
		return imagecreatefromgif($file);  // no special wrapper is required for UNIX-style operating systems
	}

	public function imagecreatefrompng($file) {
		return imagecreatefrompng($file);  // no special wrapper is required for UNIX-style operating systems
	}

	public function imagejpeg($image, $file, $quality) {
		return imagejpeg($image, $file, $quality);
	}
	
	public function imagegif($image, $file) {
		return imagegif($image, $file);
	}

	public function imagepng($image, $file, $quality) {
		return imagepng($image, $file, $quality);
	}
}

/**
* File system extensions.
* File system portability layer for Windows and UNIX-based systems.
*/
class fsx {
	private static $instance;

	public static function initialize() {
		$fs = false;
		if (class_exists('COM')) {
			try {
				$fs = new COM('Scripting.FileSystemObject', null, CP_UTF8);
			} catch (com_exception $e) { }
		}
		if ($fs !== false) {
			self::$instance = new fsx_windows($fs);  // use Windows FileSystemObject
		} else {  // COM Scripting not available
			self::$instance = new fsx_unix();  // use PHP's own functions with a thin wrapper
		}
	}

	public static function scandir($dir) {
		return self::$instance->scandir($dir);
	}

	public static function scandir_files($dir) {
		return self::$instance->scandir_files($dir);
	}

	public static function scandir_folders($dir) {
		return self::$instance->scandir_folders($dir);
	}

	public static function scandir_mtime($dir) {
		return self::$instance->scandir_mtime($dir);
	}

	public static function get_files_with_extension($dir, $ext, $rev = false) {
		if (!is_array($ext)) {
			$ext = array($ext);
		}
		return self::$instance->get_files_with_extension($dir, $ext, $rev);
	}

	/**
	* Get all files in a directory that have the specified extension sorted by last modified time.
	* @param {string} $dir Absolute path to a directory.
	* @param {string} $ext Extension to check file names against.
	* @param {boolean} $rev True if file names are to be sorted in reverse order.
	*/
	public static function get_files_with_extension_time($dir, $ext, $rev = false) {
		$files = self::$instance->get_files_with_extension($dir, $ext, $rev);

		// fetch last modified date for each file
		$times = array();
		foreach ($files as $file) {
			$times[] = self::filemtime($dir.DIRECTORY_SEPARATOR.$file);
		}

		// sort file array based on timestamp values
		$sortorder = $rev ? SORT_DESC : SORT_ASC;
		array_multisort($times, $sortorder, SORT_NUMERIC, $files, $sortorder, SORT_STRING);

		return $files;
	}

	public static function file_exists($path) {
		return self::$instance->file_exists($path);
	}

	public static function filemtime($file) {
		return self::$instance->filemtime($file);
	}

	public static function filesize($file) {
		return self::$instance->filesize($file);
	}

	public static function file_get_contents($file) {
		return self::$instance->file_get_contents($file);
	}

	public static function file_put_contents($file, $data) {
		return self::$instance->file_put_contents($file, $data);
	}

	public static function readfile($file) {
		return self::$instance->readfile($file);
	}

	public static function getimagesize($file, &$info = null) {
		return self::$instance->getimagesize($file, $info);
	}

	public static function imagecreatefromjpeg($file) {
		return self::$instance->imagecreatefromjpeg($file);
	}

	public static function imagecreatefromgif($file) {
		return self::$instance->imagecreatefromgif($file);
	}

	public static function imagecreatefrompng($file) {
		return self::$instance->imagecreatefrompng($file);
	}

	public static function imagejpeg($image, $file, $quality) {
		return self::$instance->imagejpeg($image, $file, $quality);
	}

	public static function imagegif($image, $file) {
		return self::$instance->imagegif($image, $file);
	}

	public static function imagepng($image, $file, $quality) {
		return self::$instance->imagepng($image, $file, $quality);
	}

	public static function filemdate($file) {
		return gmdate('Y-m-d H:i:s', fsx::filemtime($file));
	}
}

fsx::initialize();

}  // if (!interface_exists('fsx_functions') && !class_exists('fsx'))

/*****************************************************************************
* Directory listing
*****************************************************************************/

if (!function_exists('walkdir')) {

/**
* List files and directories inside the specified path recursively.
* @param {string} $path The directory whose files and subdirectories to list.
* @param {array} $exclude Subdirectories to exclude from the listing.
* @param {int} $depth Maximum depth to traverse the directory hierarchy; >0 for recursive directory listing with a limit, 0 for flat listing, or -1 for listing with no limit.
* @param $callback Callback function to invoke for each directory hierarchy level.
* @param {array} $ancestors Breadcrumbs for current directory ancestors returned by the callback function
*/
function walkdir($path, array $exclude = array(), $depth = 0, $callback = null, $ancestors = null) {
	$folders = fsx::scandir_folders($path);
	$files = fsx::scandir_files($path);
	if ($folders === false || $files === false) {
		return;
	}

	foreach ($folders as $key => $folder) {
		if ($folder == '.' || $folder == '..' || in_array($folder, $exclude)) {  // skip special directory entries "." and "..", as well as excluded entries
			unset($folders[$key]);
		}
	}
	foreach ($files as $key => $file) {
		if ($file{0} == '.' && in_array($file, $exclude)) {  // skip hidden files and excluded entries
			unset($files[$key]);
		}
	}

	// invoke callback function
	if (isset($callback)) {
		if (is_array($callback)) {
			$object = $callback[0];
			$params = $callback[1];
			$func = array($object, $params);
		} else {
			$func = $callback;
		}

		if (isset($ancestors)) {
			$ancestor = call_user_func($func, $path, $files, $folders, $ancestors);
			array_unshift($ancestors, $ancestor);  // add current folder to list of ancestors
		} else {
			call_user_func($func, $path, $files, $folders);
		}
	}

	// scan descandant folders
	if ($depth < 0 || $depth > 0) {
		foreach ($folders as $folder) {
			if (isset($ancestors)) {
				walkdir($path.DIRECTORY_SEPARATOR.$folder, $exclude, $depth - 1, $callback, $ancestors);
			} else {
				walkdir($path.DIRECTORY_SEPARATOR.$folder, $exclude, $depth - 1, $callback);
			}
		}
	}
}

}  // if (!function_exists('walkdir'))

/*****************************************************************************
* End of file
*****************************************************************************/
