<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

if (!function_exists('unpack_single')) {
	function unpack_single($format, $data) {
		$array = unpack($format, $data);
		return reset($array);
	}
}

/**
* A single atom (container box).
*/
class mp4atom implements metadata_node {
	/** Type selector contexts. */
	private static $atoms = array(
		'ftyp' => 'filetype',
		'free' => false,
		'mdat' => false,
		'moov' => array(  // Movie
			'prfl' => false,  // Profile
			'mvhd' => false,  // Movie header
			'clip' => array(  // Movie clipping
				'crgn' => false  // Clipping region
			),
			'udta' => array(  // User data
				'cprt' => 'copyright',
				'meta' => array(
					'hdlr' => false,
					'ilst' => array(  // "\xA9" translates to copyright symbol
						"\xA9alb" => array('data' => 'text'),  // Album
						"\xA9ART" => array('data' => 'text'),  // Artist
						'aART' => array('data' => 'text'),  // Album Artist
						"\xA9cmt" => array('data' => 'text'),  // Comment
						"\xA9day" => array('data' => 'text'),  // Year
						"\xA9nam" => array('data' => 'text'),  // Title
						"\xA9gen" => array('data' => 'text'),  // Genre
						'gnre' => array('data' => 'genre'),  // Genre
						'trkn' => array('data' => 'track'),  // Track number
						'disk' => array('data' => 'disk'),  // Disk number
						"\xA9wrt" => array('data' => 'text'),  // Composer
						"\xA9too" => array('data' => 'text'),  // Encoder
						'tmpo' => array('data' => 'tempo'),  // BPM
						'cprt' => array('data' => 'text'),  // Copyright
						'cpil' => array('data' => 'bit'),  // Compilation
						'covr' => array('data' => 'cover'),  // Artwork
						'rtng' => array('data' => 'uint8'),  // Rating/Advisory
						"\xA9grp" => array('data' => 'text'),  // Grouping
						'stik' => array('data' => 'uint8'),
						'pcst' => array('data' => 'uint8'),  // Podcast
						'catg' => array('data' => 'text'),  // Category
						'keyw' => array('data' => 'text'),  // Keyword
						'purl' => array('data' => 'text'),  // Podcast URL
						'egid' => array('data' => 'text'),  // Episode Global Unique ID
						'desc' => array('data' => 'text'),  // Description
						"\xA9lyr" => array('data' => 'text'),  // Lyrics
						'tvnn' => array('data' => 'text'),  // TV Network Name
						'tvsh' => array('data' => 'text'),  // TV Show Name
						'tven' => array('data' => 'text'),  // TV Episode Number
						'tvsn' => array('data' => 'integer'),  // TV Season
						'tves' => array('data' => 'integer'),  // TV Episode
						'purd' => array('data' => 'text'),  // Purchase Date
						'pgap' => array('data' => 'bit'),   // Gapless Playback
						'----' => array(
							'mean' => 'itunestext',
							'name' => 'itunestext',
							'data' => false
						)
					)
				)
			),
			'trak' => array(  // Track
				'tkhd' => false,  // Track header
				'clip' => array(  // Movie clipping
					'crgn' => false  // Clipping region
				),
				'matt' => array(  // Track matte
					'kmat' => false  // Compressed matte
				),
				'edts' => array(  // Edit
					'elst' => false  // Edit list
				),
				'mdia' => array(  // Media
					'mdhd' => false,  // Media header
					'hdlr' => false,  // Media handler reference
					'minf' => array(  // Media information
						'vmhd' => false,  // Video media information header
						'hdlr' => false,  // Data handler reference
						'dinf' => false,  // Data information atom
						'stbl' => array(  // Sample table
							'stts' => false,
							'stsc' => false,
							'stss' => false,
							'stsd' => false,
							'stsz' => false,
							'stco' => 'stco'
						)
					)
				)
			)
		)
	);

	/** An unqualified node name, e.g. "ilst". */
	private $name;
	/** A fully qualified node name, e.g. "moov.udta.meta.ilst". */
	private $qualified_name;
	/** Atom length in bytes. */
	private $len = -1;
	/** An array of children. Must be empty if node has data. */
	public $children = array();
	/** Atom data. Must be false if node has children. */
	public $data;

	public function __construct($qualified_name, $length = -1) {
		$this->qualified_name = $qualified_name;
		$full_name = explode('.', $qualified_name);
		$this->name = end($full_name);
		$this->len = $length;
	}

	public function __toString() {
		return $this->get_data_string();
	}

	public function get_name() {
		return $this->name;
	}

	public function get_size() {
		return $this->len;
	}

	public function get_string() {
		$s = str_replace("\xA9", '&#169;', $this->qualified_name);  // replace ANSI copyright symbol with HTML equivalent
		$s .= ' ['.$this->len.']';

		$d = $this->get_data_string();
		if ($d !== false) {
			$s .= ' '.$d;
		}
		if (!empty($this->children)) {
			$c = '';
			foreach ($this->children as $child) {
				$c .= ",\n".($child->get_string());
			}
			$s .= str_replace("\n", "\n\t", $c);
		}
		return $s;
	}

	/**
	* Performs a deep copy of the object.
	*/
	public function copy() {
		$clone = clone $this;
		$clone->children = array();
		foreach ($this->children as $child) {
			$clone->children[] = $child->copy();
		}
		return $clone;
	}

	/**
	* Type selector context for a node.
	*/
	public static function get_selector($qualified_name) {
		$parts = explode('.', $qualified_name);
		$tree = self::$atoms;
		foreach ($parts as $part) {
			if (!isset($tree[$part])) {
				return false;
			}
			$tree = $tree[$part];
		}
		return $tree;
	}

	/**
	* Node data as a string.
	*/
	protected function get_data_string() {
		return isset($this->data) ? prettify_string($this->data, true) : false;
	}

	/**
	* True if the value of the node or any of its descendants has been updated.
	*/
	public function has_changed() {
		if (isset($this->data)) {
			return true;
		}
		foreach ($this->children as $child) {
			if ($child->has_changed()) {
				return true;
			}
		}
		return false;
	}

	public function get_length() {
		return $this->len;
	}

	public function get_data() {
		return $this->data;
	}

	public function set_data($data) {
		$this->data = $data;
	}

	/**
	* Returns an HTML representation of a binary resource saved to a temporary location.
	* @param url The base URL for the temporary location.
	* @param folder A folder for temporary files in the file system to save the binary data to.
	* @return An HTML element linking to a temporary location or false on error.
	*/
	public function get_binary_html($url, $folder) {
		return false;
	}

	public function prune() {
		foreach ($this->children as $index => $child) {
			$child->prune();
			if (!($child instanceof mp4atom_unparsed) && empty($child->data) && empty($child->children)) {
				unset($this->children[$index]);
			}
		}
	}

	/**
	* Recalculate length values based on updated data and/or children.
	*/
	public function update_length() {
		if ($this->has_changed()) {
			$this->len = $this->data !== false ? strlen($this->data) : 0;
			foreach ($this->children as $child) {
				$child->update_length();
				$this->len += $child->len;
			}
			$this->len += 8;  // length (4 bytes) + unqualified name (4 bytes)
		}
	}

	/**
	* Fetch the first subtree that matches a query expression relative to the node as root.
	* The query expression takes a syntax e.g. "moov.udta.meta.ilst".
	*/
	public function fetch_single($query) {
		if (is_array($query)) {
			$keys = $query;
		} elseif (is_string($query)) {
			$keys = explode('.', $query);
		}
		return self::fetch_subtree_single($keys, $this->children);
	}

	private static function fetch_subtree_single(&$query, $items) {
		$key = current($query);
		foreach ($items as $item) {
			if ($key == $item->get_name()) {
				if (next($query) !== false) {
					return self::fetch_subtree_single($query, $item->children);
				} else {
					return $item;
				}
			}
		}
		return false;
	}

	/**
	* Fetch all metadata subtrees that match the query expression.
	*/
	public function fetch_all($query) {
		return self::fetch_subtree_all(explode('.', $query), $this->children);
	}

	private static function fetch_subtree_all($query, $items) {
		$key = array_shift($query);
		$nodes = array();
		foreach ($items as $item) {
			if ($key == $item->get_name()) {
				if (empty($query)) {
					$nodes[] = $item;
				} else {
					$nodes = array_merge($nodes, self::fetch_subtree_all($query, $item->children));
				}
			}
		}
		return $nodes;
	}

	/**
	* The relative position of the first subtree selected by a query expression relative to the start of the current node.
	*/
	public function get_position($query) {
		return self::get_subtree_offset(explode('.', $query), 0, $this->children);
	}

	private static function get_subtree_offset(&$query, $offset, $items) {
		$key = current($query);
		foreach ($items as $item) {
			if ($key == $item->get_name()) {
				if (next($query) !== false) {
					return self::get_subtree_offset($query, $offset + 8, $item->children);
				} else {
					return $offset;
				}
			}
			$offset += $item->len;
		}
		return -1;
	}

	/**
	* The subtree at the specified file offset.
	*/
	public function fetch_at($position) {
		return self::get_subtree_at($position, 0, $this->children);
	}

	private function get_subtree_at($position, $offset, $items) {
		foreach ($items as $item) {
			if ($position >= $offset && $position < $offset + $item->len) {  // position within item
				if (empty($item->children)) {
					return $item;
				} else {
					return self::get_subtree_at($position, $offset + 8, $item->children);
				}
			}
			$offset += $item->len;
		}
		return false;
	}

	public function remove_children($name) {
		foreach ($this->children as $index => $child) {
			if ($name == $child->get_name()) {
				unset($this->children[$index]);
			}
		}
		$this->children = array_values($this->children);
	}

	public function remove_descendants($name) {
		$this->remove_children($name);
		foreach ($this->children as $child) {
			$child->remove_descendants($name);
		}
	}

	/**
	* Instantiates a new node.
	*/
	public static function create($qualified_name, $atomlength = -1, $atomdata = null) {
		$atomselector = self::get_selector($qualified_name);
		$nodetype = 'mp4atom_'.$atomselector;
		if (!class_exists($nodetype)) {
			if (isset($atomdata)) {
				$atomtype = unpack_single('N', $atomdata);
				switch ($atomtype) {
					case 0:
					case 21:  // uint8
						$nodetype = 'mp4atom_uint8'; break;
					case 1:  // text
					default:
						$nodetype = 'mp4atom_text';
				}
			} else {
				$nodetype = 'mp4atom';
			}
		}
		$node = new $nodetype($qualified_name, $atomlength);
		$node->data = $atomdata;
		return $node;
	}
}

/**
* An atom whose content is copied unchanged from the source file.
*/
class mp4atom_unparsed extends mp4atom {
	/** Data offset in source file. */
	private $offset = -1;

	public function __construct($qualified_name, $length, $offset) {
		parent::__construct($qualified_name, $length);
		$this->offset = $offset;
	}

	public function get_offset() {
		return $this->offset;
	}
}

/**
* Conceptual root atom for an MPEG-4 file.
*/
class mp4atom_root extends mp4atom {
	public function __construct() {
		parent::__construct('root');
	}

	public function get_data() {
		return null;
	}

	public function get_string() {
		$s = '';
		if (!empty($this->children)) {
			foreach ($this->children as $child) {
				if (!empty($s)) {
					$s .= ",\n";
				}
				$s .= $child->get_string();
			}
		}
		return $s;
	}
}

/**
* File type atom.
*/
class mp4atom_filetype extends mp4atom {
	private static $types = array(
		'isom' => 'ISO 14496-1 Base Media',
		'iso2' => 'ISO 14496-12 Base Media',
		'mp41' => 'ISO 14496-1 version 1',
		'mp42' => 'ISO 14496-1 version 2',
		'qt  ' => 'QuickTime movie',
		'avc1' => 'JVT AVC',
		'3gp'  => '3G MP4 profile',  // fourth character varies
		'mmp4' => '3G Mobile MP4',
		'M4A ' => 'Apple AAC audio with iTunes info',
		'M4P ' => 'AES encrypted audio',
		'M4B ' => 'Apple audio with iTunes position',
		'mp71' => 'ISO 14496-12 MPEG-7 meta data');

	public function __toString() {
		$data = $this->get_data();
		return $data['type'] . (empty($data['compatible']) ? '' : ' ('.implode(', ', $data['compatible'])).')';
	}

	public function get_data() {
		$type = substr($this->data, 0, 4);
		$data = array(
			'type' => isset(self::$types[$type]) ? self::$types[$type] : $type,
			'revision' => unpack_single('N', substr($this->data, 4, 4))
		);

		// enumerate compatible types
		$data['compatible'] = array();
		for ($k = 8; $k < strlen($this->data); $k += 4) {
			$type = substr($this->data, $k, 4);
			$type3 = substr($type, 0, 3);
			if (isset(self::$types[$type])) {
				$data['compatible'][] = self::$types[$type];
			} elseif (isset(self::$types[$type3])) {
				$data['compatible'][] = self::$types[$type3];
			} elseif (ctype_print($type)) {
				$data['compatible'][] = $type;
			}
		}
		return $data;
	}

	public function set_data($value) {
		// not supported
	}

	protected function get_data_string() {
		return isset($this->data) ? prettify_string(substr($this->data, 0, 4), true).' '.prettify_hex(substr($this->data, 4, 4)).' '.prettify_string(substr($this->data, 8), true) : false;
	}
}

/**
* Sample table chunk offset atom.
*/
class mp4atom_stco extends mp4atom {
	protected function get_data_string() {
		return false;
	}
}

/**
* iTunes ASCII text atom.
*/
class mp4atom_itunestext extends mp4atom {
	public function __toString() {
		return $this->get_data();
	}

	public function get_data() {
		return substr($this->data, 4);  // cut off version information and flags
	}

	public function set_data($value) {
		$this->data = pack('Nxxxx', 0) . $value;
	}

	protected function get_data_string() {
		return isset($this->data) ? prettify_hex(substr($this->data, 0, 4)).' '.prettify_string(substr($this->data, 4), true) : false;
	}
}

/**
* Text atom with type identifier.
*/
class mp4atom_text extends mp4atom {
	public function __toString() {
		$data = $this->get_data();
		return $data ? $data : '';
	}

	public function get_data() {
		return substr($this->data, 8);  // cut off type identifier and padding
	}

	public function set_data($value) {
		$this->data = pack('Nxxxx', 1) . $value;
	}

	protected function get_data_string() {
		return isset($this->data) ? prettify_hex(substr($this->data, 0, 8)).' '.prettify_string(substr($this->data, 8), true) : false;
	}
}

/**
* Integer value atom.
*/
class mp4atom_integer extends mp4atom {
	public function __toString() {
		return (string) $this->get_data();
	}

	public function get_data() {
		return unpack_single('N', substr($this->data, 8, 4));
	}

	public function set_data($value) {
		$this->data = pack('NxxxxN', 21, $value);
	}
}

class mp4atom_uint8 extends mp4atom {
	public function get_data() {
		return substr($this->data, 8);  // cut off type identifier and padding
	}
}

/**
* Boolean value atom.
*/
class mp4atom_bit extends mp4atom {
	public function __toString() {
		return $this->get_data() ? 'Yes' : 'No';
	}

	public function get_data() {
		return unpack_single('C', substr($this->data, 8));
	}

	public function set_data($value) {
		$this->data = pack('NxxxxC', 21, (bool) $value);
	}
}

/**
* MPEG-4 standard copyright atom.
*/
class mp4atom_copyright extends mp4atom {
	/** Language code mapping from bibliographic code (B code) to terminological code (T code). */
	private static $lang_map = array(
		'alb' => 'sqi',
		'arm' => 'hye',
		'baq' => 'eus',
		'bur' => 'mya',
		'chi' => 'zho',
		'cze' => 'ces',
		'dut' => 'nld',
		'fre' => 'fra',
		'geo' => 'kat',
		'ger' => 'deu',
		'gre' => 'ell',
		'ice' => 'isl',
		'mac' => 'mkd',
		'mao' => 'mri',
		'may' => 'msa',
		'per' => 'fas',
		'rum' => 'ron',
		'slo' => 'slk',
		'tib' => 'bod',
		'wel' => 'cym');

	public function __toString() {
		$data = $this->get_data();
		if (is_array($data)) {
			return $data['text'];
		} else {
			return '';
		}
	}

	public function get_data() {
		if (!isset($this->data)) {  // not initialized
			return null;
		}
		$cprtlang = unpack_single('n', substr($this->data, 4, 2));
		$langcode = pack('CCC', (($cprtlang & 0x7c00) >> 10) + 0x60, (($cprtlang & 0x03e0) >> 5) + 0x60, (($cprtlang & 0x001f)) + 0x60);
		if (isset(self::$lang_map[$langcode])) {
			$langcode = self::$lang_map[$langcode];
		}
		return array(
			'lang' => $langcode,  // three-letter language code
			'text' => substr($this->data, 6, -1));  // strip trailing NUL byte from end
	}

	public function set_data($value) {
		if (is_array($value)) {
			$cprttext = (string) $value['text'];
			$langcode = (string) $value['lang'];
			if (!preg_match('/^[a-z]{3}$/', $langcode)) {  // invalid language code
				$langcode = 'enu';
			}
		} else {
			$cprttext = (string) $value;
			$langcode = 'enu';
		}
		$cprtlang = ((ord($langcode[0]) - 0x60) << 10) + ((ord($langcode[1]) - 0x60) << 5) + (ord($langcode[2]) - 0x60);
		$cprttext = preg_replace('/[\x80-\xff]/', '?', $cprttext);
		$this->data = pack('Cxxxn', 0, $cprtlang) . $cprttext . "\0";  // NUL-terminated string
	}

	protected function get_data_string() {
		return isset($this->data) ? prettify_hex(substr($this->data, 0, 6)).' '.prettify_string(substr($this->data, 6, -1), true).' '.prettify_hex(substr($this->data, -1, 1)) : false;
	}
}

/**
* Numeric genre atom.
*/
class mp4atom_genre extends mp4atom {
	public function __toString() {
		$data = $this->get_data();
		return $data['name'];
	}

	public function get_data() {
		$code = unpack_single('n', substr($this->data, 8));
		return array('code' => $code, 'name' => metadata_core::$genres[$code]);
	}

	public function set_data($value) {
		$this->data = pack('Nxxxxn', 0, $value);
	}

	public function get_genres() {
		return metadata_core::$genres;
	}
}

class mp4atom_count_with_total extends mp4atom {
	public function __toString() {
		$data = $this->get_data();
		return $data['number'].'/'.$data['total'];
	}

	public function get_data() {
		return unpack('nnumber/ntotal', substr($this->data, 10, 4));
	}

	public function set_data($value) {
		$intvalue = intval($value);
		if (is_array($value)) {
			$this->data = pack('Nxxxxnnnn', 0, 0, intval($value['number']), intval($value['total']), 0);
		} else {
			$this->data = pack('Nxxxxnnnn', 0, 0, $intvalue, 0, 0);
		}
	}
}

/**
* Track number and total number of tracks atom.
*/
class mp4atom_track extends mp4atom_count_with_total {
}

/**
* Disk number and total number of disks atom.
*/
class mp4atom_disk extends mp4atom_count_with_total {
}

/**
* Beats per minute atom.
*/
class mp4atom_tempo extends mp4atom {
	public function __toString() {
		return (string) $this->get_data();
	}

	public function get_data() {
		return unpack_single('n', substr($this->data, 8, 2));
	}

	public function set_data($value) {
		$this->data = pack('Nxxxxn', 21, $value);
	}
}

/**
* Cover art atom.
*/
class mp4atom_cover extends mp4atom {
	public function get_binary_html($url, $folder) {
		$data = $this->get_data();
		return get_binary_data_html($data, $url, $folder);
	}

	public function __toString() {
		$data = $this->get_data();
		if (isset($data)) {
			return '<img src="'.'data:' . $data['mime'] . ';base64,' . base64_encode($data['data']) .'" />';
		} else {
			return '';
		}
	}

	public function get_data() {
		if (!isset($this->data)) {
			return null;
		}
		$data = substr($this->data, 8);
		switch (unpack_single('N', substr($this->data, 0, 4))) {
			case 13:
				$mime = 'image/jpeg';
				break;
			case 14:
				$mime = 'image/png';
				break;
			default:
				// speculative test based on image binary data prefix
				if (substr_compare($data, "\xFF\xD8\xFF", 0, 3) === 0) {  // $JPEG = "\xFF\xD8\xFF"
					$mime = 'image/jpeg';
				} elseif (substr_compare($data, "GIF", 0, 3) === 0) {  // $GIF = "GIF"
					$mime = 'image/gif';
				} elseif (substr_compare($data, "\x89\x50\x4e\x47\x0d\x0a\x1a\x0a", 0, 8) === 0) {  // $PNG = "\x89\x50\x4e\x47\x0d\x0a\x1a\x0a"
					$mime = 'image/png';
				} else {
					// write data to temporary file and use PHP's getimagesize to find out MIME type
					$temppath = tempnam(sys_get_temp_dir(), 'meta');
					file_put_contents($temppath, $data);
					$imagedata = getimagesize($temppath);
					unlink($temppath);
					if ($imagedata !== false) {
						$mime = $imagedata['mime'];
					}
				}
		}
		return array(
			'mime' => $mime,
			'data' => $data
		);
	}

	private static function get_type_code($mime) {
		switch ($mime) {
			case 'image/jpeg': return 13;
			case 'image/png':  return 14;
		}
		return false;
	}

	public function set_data($value) {
		if (empty($value)) {
			$this->data = null;
		} elseif (is_array($value)) {
			if (!isset($value['data']) && isset($value['file'])) {
				$value['data'] = file_get_contents($value['file']);
			}
			$this->data = pack('Nxxxx', self::get_type_code($value['mime'])) . $value['data'];
		} elseif (($imagedata = getimagesize($value)) !== false) {  // assume value is a file path
			$this->data = pack('Nxxxx', self::get_type_code($imagedata['mime'])) . file_get_contents($value);
		} else {
			$this->data = null;
		}
	}
}

/**
* A searchable hierarchy of atoms.
*/
class mp4tree implements metadata_container {
	private static $metadata = array(
		'File Type' => 'ftyp',
		'Title' => "moov.udta.meta.ilst.\xA9nam.data",  // Title
		'Artist' => "moov.udta.meta.ilst.\xA9ART.data",  // Artist
		'Album' => "moov.udta.meta.ilst.\xA9alb.data",  // Album
		'Album Artist' => 'moov.udta.meta.ilst.aART.data',  // Album Artist
		'Grouping' => "moov.udta.meta.ilst.\xA9grp.data",  // Grouping
		'Year' => "moov.udta.meta.ilst.\xA9day.data",  // Year
		'Track' => 'moov.udta.meta.ilst.trkn.data',  // Track number
		'Disk' => 'moov.udta.meta.ilst.disk.data',  // Disk number
		'Composer' => "moov.udta.meta.ilst.\xA9wrt.data",  // Composer
		'Comment' => "moov.udta.meta.ilst.\xA9cmt.data",  // Comment
		'Genre' => "moov.udta.meta.ilst.\xA9gen.data",  // Genre
		'Genre Code' => 'moov.udta.meta.ilst.gnre.data',  // Genre
		'Tempo' => 'moov.udta.meta.ilst.tmpo.data',  // BPM
		'Encoder' => "moov.udta.meta.ilst.\xA9too.data",  // Encoder
		'Recording Copyright' => 'moov.udta.cprt',  // Copyright
		'Copyright' => 'moov.udta.meta.ilst.cprt.data',  // Copyright
		'Compilation' => 'moov.udta.meta.ilst.cpil.data',  // Compilation
		'Cover' => 'moov.udta.meta.ilst.covr.data',  // Artwork
		'Rating' => 'moov.udta.meta.ilst.rtng.data',  // Rating/Advisory
		'Podcast' => 'moov.udta.meta.ilst.pcst.data',  // Podcast
		'Category' => 'moov.udta.meta.ilst.catg.data',  // Category
		'Keywords' => 'moov.udta.meta.ilst.keyw.data',  // Keyword
		'Podcast URL' => 'moov.udta.meta.ilst.purl.data',  // Podcast URL
		'Episode GUID' => 'moov.udta.meta.ilst.egid.data',  // Episode Global Unique ID
		'Description' => 'moov.udta.meta.ilst.desc.data',  // Description
		'Lyrics' => "moov.udta.meta.ilst.\xA9lyr.data",  // Lyrics
		'TV Network Name' => 'moov.udta.meta.ilst.tvnn.data',  // TV Network Name
		'TV Show Name' => 'moov.udta.meta.ilst.tvsh.data',  // TV Show Name
		'TV Episode Number' => 'moov.udta.meta.ilst.tven.data',  // TV Episode Number
		'TV Season' => 'moov.udta.meta.ilst.tvsn.data',  // TV Season
		'TV Episode' => 'moov.udta.meta.ilst.tves.data',  // TV Episode
		'Purchase Date' => 'moov.udta.meta.ilst.purd.data',  // Purchase Date
		'Gapless Playback' => 'moov.udta.meta.ilst.pgap.data'   // Gapless Playback
	);

	/** The source file the tree has been constructed from. */
	public $sourcefile = false;
	public $rootnode;

	public function __construct($sourcefile, $nodes = array()) {
		$this->sourcefile = $sourcefile;
		$this->rootnode = new mp4atom_root();
		$this->rootnode->children = $nodes;
	}

	public function __toString() {
		return $this->rootnode->get_string();
	}

	public function copy() {
		$clone = new self($this->sourcefile);
		$clone->rootnode = $this->rootnode->copy();
		return $clone;
	}

	public function fetch_single($query) {
		return $this->rootnode->fetch_single($query);
	}

	public function fetch_all($query) {
		return $this->rootnode->fetch_all($query);
	}

	public function get_offset($query) {
		return $this->rootnode->get_position($query);
	}

	public function fetch_at($position) {
		return $this->rootnode->fetch_at($position);
	}

	/**
	* Optimizes the MPEG container.
	* @param tree The original tree to compute updated offset positions against.
	*/
	public function update(metadata_container $container) {
		$this->rootnode->prune();

		if (true) {  // optimize file
			// purge padding
			$this->rootnode->remove_descendants('free');

			// relocate mdat boxes
			$datablocks = array();
			foreach ($this->rootnode->children as $index => $item) {
				if ($item->get_name() == 'mdat') {
					if ($item->get_length() < 0 || $item->get_length() > 8) {
						$datablocks[] = $item;
					}
					unset($this->rootnode->children[$index]);
				}
			}
			$this->rootnode->children = array_merge($this->rootnode->children, $datablocks);
		}

		// update node content lengths
		$this->rootnode->update_length();

		// update offset values in moov.trak.mdia.minf.stbl.stco box
		$items = $this->fetch_all('moov.trak.mdia.minf.stbl.stco');
		foreach ($items as $item) {
			$offsets = unpack('N*', substr($item->data, 8));
			if (!empty($offsets)) {
				$offset_old = $container->get_offset('mdat');  // assumption: offset of mdat boxes does not change relative to one another
				$offset_new = $this->get_offset('mdat');
				$offset_diff = $offset_new - $offset_old;

				$item->data = substr($item->data, 0, 8);
				foreach ($offsets as $offset) {
					$item->data .= pack('N', $offset + $offset_diff);
				}
			}
		}
	}

	public function get_metadata_subtree() {
		return (string) $this->fetch_single('moov.udta.meta.ilst');
	}

	/**
	* Get metadata as strings.
	* @param url URL to folder where temporary files can be accessed.
	* @param folder Path to a folder for temporary files.
	*/
	public function get_metadata_strings($url = false, $folder = false) {
		$data = array();
		foreach (self::$metadata as $key => $nodequery) {
			$node = $this->fetch_single($nodequery);
			$item = false;
			if ($node !== false) {
				if ($url && $folder) {
					$item = $node->get_binary_html($url, $folder);
				}
				if ($item === false) {
					$item = (string) $node;
				}
			}
			$data[$key] = $item;
		}
		return $data;
	}

	public function get_metadata() {
		$data = array();
		foreach (self::$metadata as $key => $nodequery) {
			$node = $this->fetch_single($nodequery);
			if ($node !== false) {
				$item = $node->get_data();
			} else {
				$item = false;
			}
			$data[$key] = $item;
		}
		return $data;
	}

	public function set_metadata($metadata) {
		foreach ($metadata as $metakey => $value) {
			if (is_string($value) && strlen($value) == 0) {  // drop nodes with empty value
				$value = null;
			} elseif (is_array($value)) {
				$empty = true;
				foreach ($value as $arrayvalue) {
					if (!empty($arrayvalue)) {
						$empty = false;
						break;
					}
				}
				if ($empty) {
					$value = null;
				}
			}
			$key = self::$metadata[$metakey];
			$node = $this->fetch_single($key);
			if ($node && !isset($value)) {  // remove metadata node
				$parts = explode('.', $key);          // e.g. "moov.udta.meta.ilst.gnre.data"
				$this->fetch_single(array_slice($parts, 0, -1))->remove_children(end($parts));  // remove e.g. "data"
			} elseif (!$node && isset($value)) {  // create metadata node
				// recursively create nodes
				$parts = explode('.', $key);      // e.g. "moov.udta.meta.ilst.gnre.data"
				$items =& $this->rootnode->children;
				foreach ($parts as $index => $part) {
					$node = null;
					foreach ($items as $item) {
						if ($item->get_name() == $part) {
							$node = $item;
							break;
						}
					}
					if (!isset($node)) {
						$node = mp4atom::create(implode('.', array_slice($parts, 0, $index + 1)));
						$items[] = $node;
					}
					$items =& $node->children;
				}
			}
			if (isset($value)) {
				$node->set_data($value);
			}
		}
	}
}

/**
* A parser to extract and set ISO 14496-1 media format atoms.
* @see http://atomicparsley.sourceforge.net/mpeg-4files.html
* @see http://xhelmboyx.tripod.com/formats/mp4-layout.txt
* @see http://developer.apple.com/library/mac/#documentation/QuickTime/QTFF/QTFFChap2/qtff2.html#//apple_ref/doc/uid/TP40000939-CH204-SW1
*/
class mp4parser implements metadata_parser {
	public function parse($path) {
		if (is_readable($path)) {
			$size = filesize($path);
			$f = fopen($path, 'rb');

			// check whether of conformant file type
			if (($d = fread($f, 8)) !== false && strlen($d) == 8 && substr($d, 4, 4) == 'ftyp' && unpack_single('N', substr($d, 0, 4)) < $size) {
				fseek($f, 0, SEEK_SET);
				$data = $this->parse_boxes($f, $size, '');
				$res = new mp4tree($path, $data);
			} else {
				$res = null;
			}

			fclose($f);
			return $res;
		} else {
			return null;
		}
	}

	private function parse_boxes($f, $maxsize, $qualifier) {
		$data = array();
		$bytesleft = $maxsize;
		while ($bytesleft >= 8 && ($d = fread($f, 4)) !== false && strlen($d) > 0) {
			// determine atom length
			$atomlength = unpack_single('N', $d);
			$descendantlength = $atomlength - 8;  // compute maximum total size of descendants

			// get atom name
			$atomname = fread($f, 4);

			// extract children if any
			$qualified_name = $qualifier.$atomname;
			$offset = ftell($f) - 8;
			$atomselector = mp4atom::get_selector($qualified_name);
			if (is_array($atomselector)) {
				$dataitem = new mp4atom($qualified_name, $atomlength);

				if ($atomname == 'meta') {  // skip historical zero-length block for meta node
					$d = fread($f, 4);
					$l = unpack_single('N', $d);
					if (!$l) {
						$dataitem->data = $d;
					} else {
						fseek($f, -4, SEEK_CUR);  // rewind
					}
				}

				$dataitem->children = $this->parse_boxes($f, $descendantlength, $qualified_name.'.');
			} elseif ($atomselector !== false) {
				if ($descendantlength > 0) {
					$atomdata = fread($f, $descendantlength);
				} else {
					$atomdata = null;
				}
				$dataitem = mp4atom::create($qualified_name, $atomlength, $atomdata);
			} else {
				// skip unparsed data
				$dataitem = new mp4atom_unparsed($qualified_name, $atomlength, $offset);
				fseek($f, $descendantlength, SEEK_CUR);  // length value and name are included in total length
			}

			// add atom to atom list
			if ($dataitem) {
				$data[] = $dataitem;
			}

			$bytesleft -= $atomlength;
		}
		return $data;
	}

	/**
	* Create a new container file based on an existing container file and updated data.
	* @param container A container tree with possible updates.
	*/
	public function merge(metadata_container $container, $path) {
		$size = filesize($container->sourcefile);
		$perm = fileperms($container->sourcefile);
		$fr = fopen($container->sourcefile, 'rb');
		$temppath = tempnam(dirname($path), 'meta');
		$fw = fopen($temppath, 'wb');
		$this->merge_metadata($container->rootnode->children, $fr, $fw);
		fclose($fr);
		fclose($fw);

		if (file_exists($path)) {  // overwrite existing file
			unlink($path);
		}
		rename($temppath, $path);
		chmod($path, $perm);  // set permissions to those of original file
	}

	private function merge_metadata(&$nodes, $r, $w) {
		foreach ($nodes as $node) {
			if ($node->has_changed()) {
				fwrite($w, pack('N', $node->get_length()), 4);
				fwrite($w, $node->get_name(), 4);
				fwrite($w, $node->data);
				$this->merge_metadata($node->children, $r, $w);
			} else {
				if ($node instanceof mp4atom_unparsed) {
					fseek($r, $node->get_offset(), SEEK_SET);
					stream_copy_to_stream($r, $w, $node->get_length());
				} else {  // not found in source, should not normally happen
					fseek($w, $node->get_length(), SEEK_CUR);  // preserve integrity
				}
			}
		}
	}
}
