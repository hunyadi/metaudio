<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

if (!function_exists('unpack_single')) {
	function unpack_single($format, $data) {
		$array = unpack($format, $data);
		return reset($array);
	}
}

class mp3frame implements metadata_node {
	private static $framemappings = array(
		// ID3 version 2.2
		'COM' => 'COMM',  // Comments
		'TAL' => 'TALB',  // Album/Movie/Show title
		'TBP' => 'TBLM',  // BPM (Beats Per Minute)
		'TCM' => 'TCOM',  // Composer
		'TCO' => 'TCON',  // Content type
		'TCR' => 'TCOP',  // Copyright message
		'TDA' => 'TDAT',  // Date
		'TDY' => 'TDLY',  // Playlist delay
		'TEN' => 'TENC',  // Encoded by
		'TFT' => 'TFLT',  // File type
		'TIM' => 'TIME',  // Time
		'TKE' => 'TKEY',  // Initial key
		'TLA' => 'TLAN',  // Language(s)
		'TLE' => 'TLEN',  // Length
		'TMT' => 'TMED',  // Media type
		'TOA' => 'TOPE',  // Original artist(s)/performer(s)
		'TOF' => 'TOFN',  // Original filename
		'TOL' => 'TOLY',  // Original Lyricist(s)/text writer(s)
		'TOR' => 'TORY',  // Original release year
		'TOT' => 'TOAL',  // Original album/Movie/Show title
		'TP1' => 'TPE1',  // Lead artist(s)/Lead performer(s)/Soloist(s)/Performing group
		'TP2' => 'TPE2',  // Band/Orchestra/Accompaniment
		'TP3' => 'TPE3',  // Conductor/Performer refinement
		'TP4' => 'TPE4',  // Interpreted, remixed, or otherwise modified by
		'TPA' => 'TPOS',  // Part of a set
		'TPB' => 'TPUB',  // Publisher
		'TRC' => 'TSRC',  // ISRC (International Standard Recording Code)
		'TRD' => 'TRDA',  // Recording dates
		'TRK' => 'TRCK',  // Track number/Position in set
		'TSI' => 'TSIZ',  // Size
		'TSS' => 'TSSE',  // Software/hardware and settings used for encoding
		'TT1' => 'TIT1',  // Content group description
		'TT2' => 'TIT2',  // Title/Songname/Content description
		'TT3' => 'TIT3',  // Subtitle/Description refinement
		'TXT' => 'TEXT',  // Lyricist/text writer
		'TXX' => 'TXXX',  // User defined text information frame
		'TYE' => 'TYER',  // Year
		'WAF' => 'WOAF',  // Official audio file webpage
		'WAR' => 'WOAR',  // Official artist/performer webpage
		'WAS' => 'WOAS',  // Official audio source webpage
		'WCM' => 'WCOM',  // Commercial information
		'WCP' => 'WCOP',  // Copyright/Legal information
		'WPB' => 'WPUB',  // Publishers official webpage
		'WXX' => 'WXXX'   // User defined URL link frame
	);

	private static $frametypes = array(
		// ID3 version 2.3
		'AENC' => false,      // Audio encryption
		'APIC' => 'image',    // Attached picture
		'COMM' => 'comment',  // Comments
		'COMR' => false,      // Commercial frame
		'ENCR' => false,      // Encryption method registration
		'EQUA' => false,      // Equalization
		'ETCO' => false,      // Event timing codes
		'GEOB' => false,      // General encapsulated object
		'GRID' => false,      // Group identification registration
		'IPLS' => false,      // Involved people list
		'LINK' => false,      // Linked information
		'MCDI' => false,      // Music CD identifier
		'MLLT' => false,      // MPEG location lookup table
		'OWNE' => false,      // Ownership frame
		'PRIV' => false,      // Private frame
		'PCNT' => false,      // Play counter
		'POPM' => false,      // Popularimeter
		'POSS' => false,      // Position synchronisation frame
		'RBUF' => false,      // Recommended buffer size
		'RVAD' => false,      // Relative volume adjustment
		'RVRB' => false,      // Reverb
		'SYLT' => false,      // Synchronized lyric/text
		'SYTC' => false,      // Synchronized tempo codes
		'TALB' => false,      // Album/Movie/Show title
		'TBPM' => false,      // BPM (beats per minute)
		'TCOM' => false,      // Composer
		'TCON' => 'genre',    // Content type
		'TCOP' => false,      // Copyright message
		'TDAT' => 'date',     // Date
		'TDLY' => false,      // Playlist delay
		'TENC' => false,      // Encoded by
		'TEXT' => false,      // Lyricist/Text writer
		'TFLT' => false,      // File type
		'TIME' => 'time',     // Time
		'TIT1' => false,      // Content group description
		'TIT2' => false,      // Title/songname/content description
		'TIT3' => false,      // Subtitle/Description refinement
		'TKEY' => false,      // Initial key
		'TLAN' => false,      // Language(s)
		'TLEN' => false,      // Length
		'TMED' => false,      // Media type
		'TOAL' => false,      // Original album/movie/show title
		'TOFN' => false,      // Original filename
		'TOLY' => false,      // Original lyricist(s)/text writer(s)
		'TOPE' => false,      // Original artist(s)/performer(s)
		'TORY' => false,      // Original release year
		'TOWN' => false,      // File owner/licensee
		'TPE1' => false,      // Lead performer(s)/Soloist(s)
		'TPE2' => false,      // Band/orchestra/accompaniment
		'TPE3' => false,      // Conductor/performer refinement
		'TPE4' => false,      // Interpreted, remixed, or otherwise modified by
		'TPOS' => false,      // Part of a set
		'TPUB' => false,      // Publisher
		'TRCK' => 'track',    // Track number/Position in set
		'TRDA' => false,      // Recording dates
		'TRSN' => false,      // Internet radio station name
		'TRSO' => false,      // Internet radio station owner
		'TSIZ' => false,      // Size
		'TSRC' => false,      // ISRC (international standard recording code)
		'TSSE' => false,      // Software/Hardware and settings used for encoding
		'TYER' => false,      // Year
		'TXXX' => false,      // User defined text information frame
		'UFID' => false,      // Unique file identifier
		'USER' => false,      // Terms of use
		'USLT' => false,      // Unsychronized lyric/text transcription
		'WCOM' => false,      // Commercial information
		'WCOP' => false,      // Copyright/Legal information
		'WOAF' => false,      // Official audio file webpage
		'WOAR' => false,      // Official artist/performer webpage
		'WOAS' => false,      // Official audio source webpage
		'WORS' => false,      // Official internet radio station homepage
		'WPAY' => false,      // Payment
		'WPUB' => false,      // Publishers official webpage
		'WXXX' => false,      // User defined URL link frame

		// ID3 version 2.4
		'ASPI' => false,      // Audio seek point index
		'EQU2' => false,      // Equalization
		'RVA2' => false,      // Relative volume adjustment
		'SEEK' => false,      // Seek frame
		'SIGN' => false,      // Signature frame
		'TDEN' => false,      // Encoding time
		'TDOR' => false,      // Original release time
		'TDRC' => false,      // Recording time
		'TDRL' => false,      // Release time
		'TDTG' => false,      // Tagging time
		'TIPL' => false,      // Involved people list
		'TMCL' => false,      // Musician credits list
		'TMOO' => false,      // Mood
		'TPRO' => false,      // Produced notice
		'TSOA' => false,      // Album sort order
		'TSOP' => false,      // Performer sort order
		'TSOT' => false,      // Title sort order
		'TSST' => false       // Set subtitle
	);

	/** Four-character frame name. */
	private $name;
	/** Flags. */
	private $flags;
	/** Frame data. */
	public $data;

	public function __construct($name, $flags, $data) {
		if (strlen($name) > 4) {
			$name = substr($name, 0, 4);  // truncate to four characters
		} elseif (strlen($name) < 4) {
			$name = substr($name.'    ', 0, 4);  // pad name with spaces
		}

		$this->name = $name;
		$this->flags = $flags;
		$this->data = $data;
	}

	public function __toString() {
		return $this->get_data_string();
	}

	public function get_name() {
		return $this->name;
	}

	public function get_size() {
		return isset($this->data) ? strlen($this->data) : 0;
	}

	public function get_flags() {
		return $this->flags;
	}

	public function get_data() {
		return $this->data;
	}

	public function set_data($data) {
		$this->data = $data;
	}

	protected function get_data_string() {
		return isset($this->data) ? prettify_string($this->data, true) : false;
	}

	/**
	* Returns an HTML representation of a binary resource saved to a temporary location.
	* @param url The base URL for the temporary location.
	* @param folder A folder for temporary files in the file system to save the binary data to.
	* @return An HTML element linking to a temporary location or false on error.
	*/
	public function get_binary_html($url, $folder) {
		return false;
	}

	public function get_string() {
		$s = $this->name . ' ['.$this->get_size().']';
		$d = $this->get_data_string();
		if ($d !== false) {
			$s .= ' '.$d;
		}
		return $s;
	}

	public static function create($name, $flags = null, $data = null) {
		if (!isset($flags)) {
			if (in_array($name, array('AENC', 'ETCO', 'EQUA', 'MLLT', 'POSS', 'SYLT', 'SYTC', 'RVAD', 'TENC', 'TLEN', 'TSIZ'))) {
				$flags = pack('CC', 1 << 6, 0);
			} else {
				$flags = pack('CC', 0, 0);
			}
		}

		if (isset(self::$framemappings[$name])) {
			$name = self::$framemappings[$name];
		}

		$class = 'mp3frame';
		if (isset(self::$frametypes[$name])) {
			$classname = 'mp3frame_'.self::$frametypes[$name];
			if ($classname !== false && class_exists($classname)) {
				$class = $classname;
			} else {
				switch ($name{0}) {
					case 'T': $class = 'mp3frame_text'; break;
					case 'W': $class = 'mp3frame_url'; break;
				}
			}
		}
		return new $class($name, $flags, $data);
	}
}

class mp3frame_textual extends mp3frame {
	protected static function decode_text($selector, $text, &$encoding = null) {
		switch ($selector) {
			case "\x00": $encoding = 'ISO-8859-1'; break;
			case "\x01": $encoding = 'UTF-16'; break;
			case "\x02": $encoding = 'UTF-16BE'; break;
			case "\x03": $encoding = 'UTF-8';
		}
		if (isset($encoding) && $encoding != 'UTF-8') {
			return iconv($encoding, 'UTF-8', $text);
		} else {
			return $text;  // no decoding performed
		}
	}
}

class mp3frame_text extends mp3frame_textual {
	public function __toString() {
		return $this->data ? $this->get_data_text() : '';
	}

	public function get_data() {
		return $this->get_data_text();
	}

	public function set_data($text) {
		if (is_utf8_iso8859($text)) {  // can be represented with ISO-8859-1
			$this->data = "\x00" . iconv('UTF-8', 'ISO-8859-1', $text);
		} else {  // cannot be represented with ISO-8859-1
			$this->data = "\x01" . iconv('UTF-8', 'UTF-16', $text);
		}
	}

	protected function get_data_text(&$encoding = null) {
		return $this->data ? self::decode_text($this->data{0}, substr($this->data, 1), $encoding) : false;
	}

	protected function get_data_string() {
		if ($this->data) {
			$text = $this->get_data_text($encoding);
			return '['.$encoding.'] '.prettify_string($text, true);
		} else {
			return false;
		}
	}
}

class mp3frame_genre extends mp3frame_text {
	public function set_data($data) {
		if (is_numeric($data)) {
			$code = intval($data);
			if (isset(metadata_core::$genres[$code])) {
				$text = '('.($code-1).')'.metadata_core::$genres[$code];  // ID3 genre codes start from 0
			} else {
				$text = $data;
			}
		} else {
			$text = $data;
		}
		parent::set_data($text);
	}
}

class mp3frame_track extends mp3frame_text {
	public function get_data() {
		$text = parent::get_data();
		$p = strpos($text, '/');
		if ($p !== false) {
			return array(
				'number' => substr($text, 0, $p),
				'total' => substr($text, $p+1)
			);
		} else {
			return $text;
		}
	}

	public function set_data($data) {
		if (is_array($data)) {
			$text = $data['number'].'/'.$data['total'];
		} else {
			$text = $data;
		}
		parent::set_data($text);
	}
}

class mp3frame_year extends mp3frame_text {
	public function set_data($year) {
		if (preg_match('/^\d+$/', $year)) {
			parent::set_data($year);
		}
	}
}

/**
* Date frame.
* The frame has the following structure:
* - Identifier                 "TDAT"
* - Date of the recording      DDMM
*/
class mp3frame_date extends mp3frame_text {
	public function __toString() {
		$date = $this->get_data();
		if (is_array($date)) {
			return $date['month'].'-'.$date['day'];
		} else {
			return $date ? $date : '';
		}
	}

	public function get_data() {
		$text = parent::get_data();
		if (strlen($text) == 4) {
			return array(
				'month' => substr($text, 2, 2),
				'day' => substr($text, 0, 2)
			);
		} else {
			return $text;
		}
	}

	private function set_date($month, $day) {
		if ($month >= 1 && $month <= 12 && $day >= 1 && $day <= 31) {
			parent::set_data(sprintf('%02u%02u', $day, $month));
		}
	}

	public function set_data($date) {
		if (is_array($date)) {
			$this->set_date($date['month'], $date['day']);
		} elseif (is_string($date)) {
			$matches = false;
			if (preg_match('#^([0-9]{1,2})/([0-9]{1,2})$#', $date, $matches)) {  // DD/MM
				$this->set_date($matches[2], $matches[1]);
			} elseif (preg_match('#^([0-9]{1,2})-([0-9]{1,2})$#', $date, $matches)) {  // MM-DD
				$this->set_date($matches[1], $matches[2]);
			}
		}
	}
}

/**
* Time frame.
* The frame has the following structure:
* - Identifier                 "TIME"
* - Time of the recording      HHMM
*/
class mp3frame_time extends mp3frame_text {
	public function __toString() {
		$time = $this->get_data();
		if (is_array($time)) {
			return $time['hour'].':'.$time['minute'];
		} else {
			return $time ? $time : '';
		}
	}

	public function get_data() {
		$text = parent::get_data();
		if (strlen($text) == 4) {
			return array(
				'hour' => substr($text, 0, 2),
				'minute' => substr($text, 2, 2)
			);
		} else {
			return $text;
		}
	}

	private function set_time($hour, $minute) {
		if ($hour >= 0 && $hour < 24 && $minute >= 0 && $minute < 60) {
			parent::set_data(sprintf('%02u%02u', $hour, $minute));
		}
	}

	public function set_data($time) {
		if (is_array($time)) {
			$this->set_time($time['hour'], $time['minute']);
		} elseif (is_string($time)) {
			$matches = false;
			if (preg_match('#^([0-9]{1,2})[:-]([0-9]{1,2})$#', $time, $matches)) {  // HH:MM or HH-MM
				$this->set_time($matches[1], $matches[2]);
			}
		}
	}
}

/**
* Copyright message frame.
* Text in this frame should be preceded with the HTML text "Copyright &#169;".
*/
class mp3frame_copyright extends mp3frame_text {
	public function set_data($text) {
		$text = preg_replace('/^\s*Copyright\s*/', '', $text, 1);
		parent::set_data($text);
	}
}

/**
* Comment frame.
* The frame has the following structure:
* - Identifier                 "COMM"
* - Text encoding              $xx
* - Language                   $xx xx xx
* - Short content description  <text string according to encoding> $00 (00)
* - The actual text            <full text string according to encoding>
*/
class mp3frame_comment extends mp3frame_textual {
	public function __toString() {
		return $this->data ? trim($this->get_data_text()) : '';
	}

	public function get_data() {
		return $this->get_data_text();
	}

	public function set_data($data) {
		if (is_array($data)) {
			$language = $data['lang'];
			$text = $data['summary'] . "\x00" . $data['text'];
		} else {
			$language = 'eng';
			$text = "\x00" . $data;
		}

		if (is_utf8_iso8859($text)) {  // can be represented with ISO-8859-1
			$this->data = "\x00" . $language . iconv('UTF-8', 'ISO-8859-1', $text);
		} else {  // cannot be represented with ISO-8859-1
			$this->data = "\x01" . $language . iconv('UTF-8', 'UTF-16', $text);
		}
	}

	protected function get_data_text(&$encoding = null) {
		if (!$this->data) {
			return false;
		}
		$text = self::decode_text($this->data{0}, substr($this->data, 4), $encoding);
		return str_replace("\x00", "\n", $text);
	}

	protected function get_data_string() {
		if ($this->data) {
			$text = self::decode_text($this->data{0}, substr($this->data, 4), $encoding);
			$nulpos = strpos($text, "\x00");
			if ($nulpos !== false) {
				$pretty = prettify_string(substr($text, 0, $nulpos), true) .' '. prettify_hex("\x00") .' '. prettify_string(substr($text, $nulpos + 1), true);
			} else {
				$pretty = prettify_string($text, true);
			}
			return '['.$encoding.'] ' . $pretty;
		} else {
			return false;
		}
	}
}

class mp3frame_url extends mp3frame {
	public function __toString() {
		return $this->data ? $this->data : '';
	}
}

class mp3frame_image extends mp3frame {
	public function get_binary_html($url, $folder) {
		$data = $this->get_data();
		return get_binary_data_html($data, $url, $folder);
	}

	public function __toString() {
		$data = $this->get_data();
		if (isset($data)) {
			$alt = isset($data['description']) ? 'alt="' . $data['description'] . '" ' : '';
			return '<img ' . $alt . 'src="'.'data:' . $data['mime'] . ';base64,' . base64_encode($data['data']) .'" />';
		} else {
			return '';
		}
	}

	public function get_data() {
		if (!isset($this->data)) {
			return null;
		}

		$encodingtype = $this->data{0};
		$end_mime = strpos($this->data, "\x00", 1);     // position of NUL-terminator that ends MIME type
		$mime = substr($this->data, 1, $end_mime - 1);  // MIME type
		$picturetype = $this->data{$end_mime + 1};      // picture type
		switch ($encodingtype) {
			case "\x00": $encoding = 'ISO-8859-1'; $terminator = "\x00"; break;  // encoding and terminator that ends description
			case "\x01": $encoding = 'UTF-16';     $terminator = "\x00\x00"; break;
			case "\x02": $encoding = 'UTF-16BE';   $terminator = "\x00\x00"; break;
			case "\x03": $encoding = 'UTF-8';      $terminator = "\x00";
		}
		$end_description = strpos($this->data, $terminator, $end_mime + 2);   // skip MIME type NUL-terminator and picture type byte
		$description = substr($this->data, $end_mime + 2, $end_description - $end_mime);
		$blob = substr($this->data, $end_description + strlen($terminator));  // skip terminator that ends description
		if (isset($encoding) && $encoding != 'UTF-8') {
			$description = iconv($encoding, 'UTF-8', $description);
		}

		return array(
			'mime' => $mime,
			'type' => $picturetype,
			'text' => $description,
			'data' => $blob
		);
	}

	public function set_data($value) {
		if (empty($value)) {
			$this->data = null;
		} elseif (is_array($value)) {
			// encoding and encoded description text
			$text = isset($value['text']) ? $value['text'] : '';
			$text .= "\x00";
			if (is_utf8_iso8859($text)) {  // can be represented with ISO-8859-1
				$encodingtype = "\x00";
				$description = iconv('UTF-8', 'ISO-8859-1', $text);
			} else {  // cannot be represented with ISO-8859-1
				$encodingtype = "\x01";
				$description = iconv('UTF-8', 'UTF-16', $text);
			}

			// MIME type and image data
			$mime = isset($value['mime']) ? $value['mime'] : 'image/jpeg';
			if (!isset($value['data']) && isset($value['file'])) {
				$blob = file_get_contents($value['file']);
				if (!isset($mime)) {
					$imagedata = getimagesize($value['file']);
					$mime = $imagedata['mime'];
				}
			}

			// picture type
			$picturetype = isset($value['type']) ? $value['type'] : "\x03";  // cover (front)

			$this->data = $encodingtype . $mime . "\x00" . $picturetype . $description . $blob;
		} elseif (($imagedata = getimagesize($value)) !== false) {  // assume value is a file path
			$this->data = "\x00" . $imagedata['mime'] . "\x00\x03\x00" . file_get_contents($value);
		} else {
			$this->data = null;
		}
	}
}

class mp3container implements metadata_container {
	private static $metadata = array(
		'Title' => 'TIT2',  // Title
		'Artist' => 'TPE1',  // Artist
		'Album' => 'TALB',  // Album
		'Grouping' => 'TIT1',  // Grouping
		'Year' => 'TYER',  // Year YYYY
		'Date' => 'TDAT',  // Date DDMM
		'Time' => 'TIME',  // Time HHMM
		'Track' => 'TRCK',  // Track number
		'Composer' => 'TCOM',  // Composer
		'Comment' => 'COMM',  // Comment
		'Genre Code' => 'TCON',  // Genre code [must be before "Genre" to be overridden]
		'Genre' => 'TCON',  // Genre
		'Tempo' => 'TBPM',  // BPM
		'Encoder' => 'TENC',  // Encoder
		'Copyright' => 'TCOP',  // Copyright
		'Cover' => 'APIC',  // Artwork
		'Copyright URL' => 'WCOP',  // Copyright/Legal information
		'Audio Webpage' => 'WOAF',  // Official audio file webpage
		'Artist Webpage' => 'WOAR',  // Official artist/performer webpage
		'Audio Source Webpage' => 'WOAS'  // Official audio source webpage
	);

	public $sourcefile = false;
	public $frames = array();

	public function __construct($sourcefile, $frames = array()) {
		$this->sourcefile = $sourcefile;
		$this->frames = $frames;
	}

	public function copy() {
		$clone = new self($this->sourcefile);
		$clone->frames = array();
		foreach ($this->frames as $key => $frame) {
			$clone->frames[$key] = $frame;
		}
		return $clone;
	}

	public function get_metadata_strings($url = false, $folder = false) {
		$data = array();
		foreach (self::$metadata as $key => $name) {
			if (isset($this->frames[$name])) {
				$frame = $this->frames[$name];
				if ($url && $folder) {
					$value = $frame->get_binary_html($url, $folder);
				}
				if ($value === false) {
					$value = (string) $frame;
				}
			} else {
				$value = false;
			}
			$data[$key] = $value;
		}
		return $data;
	}

	public function get_metadata() {
		$data = array();
		foreach (self::$metadata as $key => $name) {
			if (isset($this->frames[$name])) {
				$item = $this->frames[$name]->get_data();
			} else {
				$item = false;
			}
			$data[$key] = $item;
		}
		return $data;
	}

	public function set_metadata($metadata) {
		if (!empty($metadata['Genre Code']) && empty($metadata['Genre'])) {
			unset($metadata['Genre']);  // do not remove genre code if no text genre is set
		}

		foreach ($metadata as $metakey => $value) {
			if (is_string($value) && strlen($value) == 0) {  // drop nodes with empty value
				$value = null;
			} elseif (is_array($value)) {
				$empty = true;
				foreach ($value as $arrayvalue) {
					if (!empty($arrayvalue)) {
						$empty = false;
						break;
					}
				}
				if ($empty) {
					$value = null;
				}
			}

			$key = self::$metadata[$metakey];
			if (isset($this->frames[$key]) && !isset($value)) {  // remove metadata node
				unset($this->frames[$key]);
			} elseif (!isset($this->frames[$key]) && isset($value)) {  // create metadata node
				$this->frames[$key] = mp3frame::create($key);
			}
			if (isset($value)) {
				$this->frames[$key]->set_data($value);
			}
		}
	}

	public function update(metadata_container $container) {

	}

	public function __toString() {
		$s = '';
		foreach ($this->frames as $frame) {
			if (!empty($s)) {
				$s .= ",\n";
			}
			$s .= $frame->get_string();
		}
		return $s;
	}
}

class mp3parser implements metadata_parser {
	public function parse($path) {
		if (is_readable($path)) {
			$f = fopen($path, 'rb');

			// check whether of conforming file type
			if (($d = fread($f, 10)) !== false && strlen($d) == 10) {
				if ($this->parse_header($d, $headerdata)) {
					switch ($headerdata['version']) {
						case 2:
						case 3:
						case 4:
							$frames = $this->parse_frames($f, $headerdata);
							$res = new mp3container($path, $frames);
							break;
						default:  // ID3 version and/or revision not supported
							$res = null;
					}
				} else {  // ID3 data not present
					$res = new mp3container($path, array());
				}
			} else {
				$res = null;
			}

			fclose($f);
			return $res;
		} else {
			return null;
		}
	}

	/**
	* Parses ID3v2 header (10 bytes).
	*/
	private function parse_header($header, &$data = null) {
		if (substr($header, 0, 3) != 'ID3') {  // ID3v2 file identifier or ID3v2 version mismatch
			return false;
		}
		$data = array();

		// ID3v2 version
		$data['version'] = unpack_single('C', $header{3});
		$data['revision'] = unpack_single('C', $header{4});

		// ID3v2 flags
		$flags = unpack_single('C', $header{5});
		$data['unsynchronization'] = ($flags >> 7) & 0x01;  // Unsynchronization
		$data['extended'] = ($flags >> 6) & 0x01;           // Extended header
		$data['experimental'] = ($flags >> 5) & 0x01;       // Experimental indicator
		if ($data['extended'] || $data['experimental']) {
			return false;  // not supported
		}

		// ID3v2 size
		list($size1, $size2, $size3, $size4) = array_values(unpack('C4', substr($header, 6, 4)));
		if (($size1 & 0x80) || ($size2 & 0x80) || ($size3 & 0x80) || ($size4 & 0x80)) {  // first bit for size bytes should be zero
			return false;
		}
		$data['size'] = ($size1 << 21) + ($size2 << 14) + ($size3 << 7) + $size4;
		return true;
	}

	private function parse_frames($f, $headerdata) {
		$size = $headerdata['size'];
		switch ($headerdata['version']) {
			case 2:
				$frames = array();
				while ($size > 0) {
					// read frame header and apply unsynchronization scheme
					$frameheader = self::read_file_data($f, $headerdata, 6);  // frame identifier + size
					if (strlen($frameheader) < 6) {
						break;
					}

					// process frame header
					$frameid = substr($frameheader, 0, 3);
					$unpack = unpack('C3data', substr($frameheader, 3, 3));
					$unpack = array_values($unpack);  // get rid of associative array keys and convert into a numeric array
					$framesize = ($unpack[0] << 16) | ($unpack[1] << 8) | $unpack[2];
					if ($framesize < 1) {  // insufficient frame size
						break;
					} elseif ($framesize > $size) {
						$framesize = $size;  // safety measure to avoid excessive memory allocation failures on malformed files
					}

					// read frame data and apply unsynchronization scheme
					$framedata = self::read_file_data($f, $headerdata, $framesize);

					// process frame data
					$frame = mp3frame::create($frameid, null, $framedata);
					$frames[$frame->get_name()] = $frame;  // name may change due to ID3v2.2 to ID3v2.3 mapping
					$size -= $framesize;
				}
				return $frames;
			case 3:
			case 4:
				$frames = array();
				while ($size > 0) {
					// read frame header and apply unsynchronization scheme
					$frameheader = self::read_file_data($f, $headerdata, 10);  // frame identifier + size + flags
					if (strlen($frameheader) < 10) {
						break;
					}

					// process frame header
					$frameid = substr($frameheader, 0, 4);
					$framesize = unpack_single('N', substr($frameheader, 4, 4));
					if ($framesize < 1) {  // insufficient frame size
						break;
					} elseif ($framesize > $size) {
						$framesize = $size;  // safety measure to avoid excessive memory allocation failures on malformed files
					}

					$frameflags = substr($frameheader, 8, 2);

					// read frame data and apply unsynchronization scheme
					$framedata = self::read_file_data($f, $headerdata, $framesize);

					// process frame data
					$frames[$frameid] = mp3frame::create($frameid, $frameflags, $framedata);
					$size -= $framesize;
				}
				return $frames;
		}
	}

	private static function read_file_data($f, $headerdata, $len) {
		if ($headerdata['unsynchronization']) {
			$nextchar = false;
			$buffer = array();
			$k = 0;  // actual characters read so far (excluding unsynchronization sequence bytes)
			while ($k < $len) {
				$prevchar = $nextchar;
				$nextchar = fgetc($f);
				if ($prevchar != "\xFF" || $nextchar != "\x00") {  // replace unsynchronization sequences of FF 00 with FF
					$buffer[] = $nextchar;
					$k++;
				}
			}
			$data = implode($buffer);
		} else {
			$data = fread($f, $len);
		}
		return $data;
	}

	public function merge(metadata_container $container, $path) {
		$perm = fileperms($container->sourcefile);

		// open files
		$fr = fopen($container->sourcefile, 'rb');
		$temppath = tempnam(dirname($path), 'tmp');
		$fw = fopen($temppath, 'wb');

		// skip ID3 data in source file
		if (($d = fread($fr, 10)) !== false && strlen($d) == 10 && $this->parse_header($d, $headerdata)) {
			fseek($fr, $headerdata['size'], SEEK_CUR);  // seek forward to skip bytes of ID3 frames
		} else {
			fseek($fr, 0, SEEK_SET);
		}

		// compute ID3 size
		$size = 0;
		foreach ($container->frames as $frame) {
			$size += 10 + $frame->get_size();  // frame header size (10 bytes) + frame data size
		}
		fwrite($fw, 'ID3');
		fwrite($fw, pack('CCC', 3, 0, 0));  // ID3 version + ID3 revision + ID3 flags
		fwrite($fw, pack('CCCC', ($size >> 21) & 0x7f, ($size >> 14) & 0x7f, ($size >> 7) & 0x7f, $size & 0x7f));

		// write ID3 data to destination file
		foreach ($container->frames as $frame) {
			fwrite($fw, $frame->get_name());
			fwrite($fw, pack('N', $frame->get_size()));
			fwrite($fw, $frame->get_flags());
			fwrite($fw, $frame->data);
		}

		// copy remaining data from source file to destination file
		stream_copy_to_stream($fr, $fw);

		// close files
		fclose($fr);
		fclose($fw);

		if (file_exists($path)) {  // overwrite existing file
			unlink($path);
		}
		rename($temppath, $path);
		chmod($path, $perm);  // set permissions to those of original file
	}
}
