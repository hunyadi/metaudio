<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

$codepages = array(
	'ascii' => 'ASCII',
	'iso-8859-1' => 'ISO 8859-1 Western Europe',
	'iso-8859-2' => 'ISO 8859-2 Western and Central Europe',
	'iso-8859-3' => 'ISO 8859-3 Western Europe and South European',
	'iso-8859-4' => 'ISO 8859-4 Western Europe and Baltic',
	'iso-8859-5' => 'ISO 8859-5 Cyrillic alphabet',
	'iso-8859-6' => 'ISO 8859-6 Arabic',
	'iso-8859-7' => 'ISO 8859-7 Greek',
	'iso-8859-8' => 'ISO 8859-8 Hebrew',
	'iso-8859-9' => 'ISO 8859-9 Western Europe with amended Turkish',
	'iso-8859-10' => 'ISO 8859-10 Western Europe with Nordic',
	'iso-8859-11' => 'ISO 8859-11 Thai',
	'iso-8859-13' => 'ISO 8859-13 Baltic languages and Polish',
	'iso-8859-14' => 'ISO 8859-14 Celtic languages',
	'iso-8859-15' => 'ISO 8859-15 ISO 8859-1 with Euro sign',
	'iso-8859-16' => 'ISO 8859-16 Central, Eastern and Southern European',
	'windows-1250' => 'Windows-1250 Central European Latin',
	'windows-1251' => 'Windows-1251 Cyrillic alphabets',
	'windows-1252' => 'Windows-1252 Western languages',
	'windows-1253' => 'Windows-1253 Greek',
	'windows-1254' => 'Windows-1254 Turkish',
	'windows-1255' => 'Windows-1255 Hebrew',
	'windows-1256' => 'Windows-1256 Arabic',
	'windows-1257' => 'Windows-1257 Baltic languages',
	'windows-1258' => 'Windows-1258 Vietnamese'
);