<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once 'helper.php';

jimport( 'joomla.application.component.model' );

/**
* metaudio Model.
*/
class metaudioModelRecording extends JModelLegacy {
	public function isEditable($filepath) {
		return metadata_core::is_editable($filepath);
	}

	/**
	* Loads all metadata of an audio recording from the database cache.
	* @param path A relative path to the recording w.r.t. the Joomla root.
	*/
	public function queryMetadata($path) {
		$filename = basename($path);
		$folderpath = dirname($path);

		// scan for changes to file
		metaudioHelper::scan($folderpath, array($filename));

		$db = JFactory::getDBO();
		$db->setQuery(
			'SELECT `propertyname`, `textvalue` '.
			'FROM `#__metaudio_data` AS d '.
			'INNER JOIN `#__metaudio_property` AS p ON d.propertyid = p.propertyid '.
			'INNER JOIN `#__metaudio_file` AS f ON d.fileid = f.fileid '.
			'INNER JOIN `#__metaudio_folder` AS g ON f.folderid = g.folderid '.
			'WHERE f.filename = '.$db->quote($filename).' AND g.folderpath = '.$db->quote($folderpath)
		);
		$metadata = $db->loadAssocList('propertyname');
		foreach ($metadata as $property => &$value) {
			$value = $value['textvalue'];  // remove wrapper array
		}
		unset($value);
		return $metadata;
	}

	/**
	* Loads all metadata of an audio recording.
	* @param path A relative path to the recording w.r.t. the Joomla root.
	*/
	public function loadMetadata($path, &$metadata = null, &$container = null) {
		$filepath = metaudioHelper::getPath($path);
		$container = metadata_core::parse($filepath);
		if ($container) {
			$metastrings = metaudioHelper::getMetadataStrings($container);
			$metadata = $container->get_metadata();
			return $metastrings;
		} else {
			return null;
		}
	}

	/**
	* Updates metadata of an audio recording.
	* @param path A relative path to the recording w.r.t. the Joomla root.
	*/
	public function updateRecording($path, $newpath, $metadata) {
		metadata_core::update(metaudioHelper::getPath($path), metaudioHelper::getPath($newpath), $metadata);
	}
}
