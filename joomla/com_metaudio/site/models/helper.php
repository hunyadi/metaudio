<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'core.php';
require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'filesystem.php';

class metaudioHelper {
	/**
	* Builds an absolute path from a relative path.
	*/
	public static function getPath($path) {
		return JPATH_ROOT.DIRECTORY_SEPARATOR.('/' != DIRECTORY_SEPARATOR ? str_replace('/', DIRECTORY_SEPARATOR, $path) : $path);
	}

	public static function getMetadataStrings($container) {
		// create cache directory on demand
		$cachedir = JPATH_ROOT.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.'com_metaudio';
		if (!is_dir($cachedir)) {
			if (mkdir($cachedir, 0755)) {
				// create an index.html to prevent getting a web directory listing
				@file_put_contents($cachedir.DIRECTORY_SEPARATOR.'index.html', '<html><body bgcolor="#FFFFFF"></body></html>');
			}
		}

		// fetch metadata strings
		return $container->get_metadata_strings(JURI::base(true).'/cache/com_metaudio', $cachedir);
	}

	/**
	* Updates the file metadata cache by inspecting file timestamps.
	* @param folder A relative folder path w.r.t. the Joomla root.
	*/
	public static function scan($folder, $files) {
		$folderid = self::queryFolder($folder);
		$dbfiles = self::queryFiles($folderid);

		foreach ($files as $file) {
			$filepath = self::getPath($folder.'/'.$file);
			if (isset($dbfiles[$file]) && self::isUpdated($filepath, $dbfiles[$file]['filetime'])) {  // file exists in database but has changed since it was added
				$fileid = $dbfiles[$file]['fileid'];
				self::updateFileMetadata($fileid, $folder, $file);
			} elseif (!isset($dbfiles[$file])) {
				self::addFileMetadata($folderid, $folder, $file);
			}
		}
	}

	/**
	* Last time a file was modified.
	* @param filepath An absolute path to the file to check.
	*/
	private static function getModificationTime($filepath) {
		$filetime = @fsx::filemtime($filepath);  // stat can fail if $filepath contains non-ASCII characters
		return $filetime !== false ? gmdate('Y-m-d H:i:s', $filetime) : false;  // format as 'YYYY-MM-DD HH:MM:SS'
	}

	/**
	* Check if a file has been modified since the specified time.
	* @param filepath An absolute path to the file to check.
	* @param oldtime A UTC date and time formatted as 'YYYY-MM-DD HH:MM:SS'.
	*/
	private static function isUpdated($filepath, $oldtime) {
		$curtime = self::getModificationTime($filepath);
		return $curtime !== false ? strcmp($curtime, $oldtime) > 0 : false;  // if cannot determine whether file has been updated, assume not
	}

	//
	// SQL methods
	//

	/**
	* Database identifier of a folder.
	*/
	private static function queryFolder($folder) {
		$db = JFactory::getDBO();
		$db->setQuery('SELECT `folderid` FROM `#__metaudio_folder` WHERE `folderpath` = '.$db->quote($folder));
		$folderid = $db->loadResult();
		if (!$folderid) {
			$db->setQuery('INSERT INTO `#__metaudio_folder` (`folderpath`) VALUES ('.$db->quote($folder).')');
			$db->execute();
			$folderid = $db->insertid();
		}
		return $folderid;
	}

	/**
	* List of files in a folder.
	*/
	private static function queryFiles($folderid) {
		$db = JFactory::getDBO();
		$db->setQuery('SELECT `fileid`, `filename`, `filetime` FROM `#__metaudio_file` WHERE `folderid` = '.$folderid);
		return $db->loadAssocList('filename');
	}

	/**
	* Updates metadata for a file in the database.
	*/
	private static function updateMetadata($fileid, $folder, $file) {
		// remove metadata entries for file
		$db = JFactory::getDBO();
		$db->setQuery('DELETE FROM `#__metaudio_data` WHERE `fileid` = '.$fileid);
		$db->execute();

		// parse file
		$filepath = self::getPath($folder.'/'.$file);
		$container = metadata_core::parse($filepath);

		if ($container) {
			$data = self::getMetadataStrings($container);

			// add metadata entries for file
			foreach ($data as $propertyname => $value) {
				if (!empty($value)) {
					$db->setQuery(
						'INSERT INTO `#__metaudio_data` (`fileid`, `propertyid`, `textvalue`) SELECT '.$fileid.', `propertyid`, '.$db->quote($value).' FROM `#__metaudio_property` WHERE `propertyname` = '.$db->quote($propertyname)
					);
					$db->execute();
				}
			}
		}
	}

	/**
	* Updates metadata for a file already in the database cache.
	*/
	private static function updateFileMetadata($fileid, $folder, $file) {
		$filepath = self::getPath($folder.'/'.$file);

		// START TRANSACTION
		self::updateMetadata($fileid, $folder, $file);
		$db = JFactory::getDBO();
		$db->setQuery(
			'UPDATE `#__metaudio_file` SET `filetime` = '.$db->quote(self::getModificationTime($filepath)).' WHERE `fileid` = '.$fileid
		);
		$db->execute();
		// COMMIT
	}

	/**
	* Adds metadata for a new file not yet in the database cache.
	*/
	private static function addFileMetadata($folderid, $folder, $file) {
		$filepath = self::getPath($folder.'/'.$file);

		// START TRANSACTION
		$db = JFactory::getDBO();
		$db->setQuery(
			'INSERT INTO `#__metaudio_file` (`folderid`, `filename`, `filetime`) VALUES ('.$folderid.', '.$db->quote($file).', '.$db->quote(self::getModificationTime($filepath)).')'
		);
		$db->execute();
		self::updateMetadata($db->insertid(), $folder, $file);
		// COMMIT
	}

	//
	// End of SQL methods
	//
}
