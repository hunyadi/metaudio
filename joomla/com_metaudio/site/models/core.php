<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

/**
* Whether a UTF-8 text string can be represented in the ISO-8859-1 character set.
*/
function is_utf8_iso8859($text) {
	return strlen(iconv('UTF-8', 'ISO-8859-1//IGNORE', $text)) == iconv_strlen($text, 'UTF-8');
	// return !preg_match('/[^\x00-\x7F]/', $text);  // US-ASCII implies ISO-8859-1
}

/**
* Pretty-prints data as a sequence of hex-encoded characters.
*/
function prettify_hex($string) {
	$chars = array();
	$len = strlen($string);
	for ($k = 0; $k < $len; $k++) {
		$chars[] = '0x'.sprintf('%02x', ord($string[$k]));
	}
	return implode(' ', $chars);
}

/**
* Pretty-prints data stored in a string.
* Strings that contain binary values are represented as a series of hex character codes.
*/
function prettify_string($string, $quote = false) {
	if (preg_match('/[\x00-\x08\x0e-\x1f]/', $string)) {
		if (strlen($string) > 100) {  // show first 100 characters only
			return prettify_hex(substr($string, 0, 100)) . ' ...';
		} else {
			return prettify_hex($string);
		}
	} else {
		//$string = str_replace(array("\r","\n"), array('\r','\n'), $string);
		return ($quote ? '"'.$string.'"' : $string);
	}
}

/**
* Returns an HTML representation of a binary resource saved to a temporary location.
* @param data The binary data to save as an array with keys 'data' and 'mime'.
* @param url The base URL for the temporary location.
* @param folder A folder for temporary files in the file system to save the binary data to.
* @return An HTML element linking to a temporary location or false on error.
*/
function get_binary_data_html($data, $url, $folder) {
	if (!$data) {
		return false;
	}
	$file = md5($data['data']);
	switch ($data['mime']) {
		case 'image/jpeg'; $file .= '.jpg'; break;
		case 'image/png';  $file .= '.png'; break;
		case 'image/gif';  $file .= '.gif'; break;
	}
	$destination = $folder.DIRECTORY_SEPARATOR.$file;
	if (file_exists($destination) || file_put_contents($destination, $data['data'])) {
		$imagedata = getimagesize($destination);
		return '<img src="'.$url.'/'.$file.'" '.$imagedata[3].' />';
	} else {
		return false;
	}
}

interface metadata_node {
	public function get_size();
	public function get_data();
	public function set_data($data);
	public function get_binary_html($url, $folder);
}

interface metadata_container {
	public function copy();
	public function update(metadata_container $container);
	public function get_metadata();
	public function set_metadata($metadata);
	public function get_metadata_strings($url = false, $folder = false);
}

interface metadata_parser {
	public function parse($path);
	public function merge(metadata_container $container, $path);
}

class metadata_core {
	public static $genres = array(
		1 => 'Blues',
		'Classic Rock',
		'Country',
		'Dance',
		'Disco',
		'Funk',
		'Grunge',
		'Hip-Hop',
		'Jazz',
		'Metal',
		'New Age',
		'Oldies',
		'Other',
		'Pop',
		'R&B',
		'Rap',
		'Reggae',
		'Rock',
		'Techno',
		'Industrial',
		'Alternative',
		'Ska',
		'Death Metal',
		'Pranks',
		'Soundtrack',
		'Euro-Techno',
		'Ambient',
		'Trip-Hop',
		'Vocal',
		'Jazz+Funk',
		'Fusion',
		'Trance',
		'Classical',
		'Instrumental',
		'Acid',
		'House',
		'Game',
		'Sound Clip',
		'Gospel',
		'Noise',
		'AlternRock',
		'Bass',
		'Soul',
		'Punk',
		'Space',
		'Meditative',
		'Instrumental Pop',
		'Instrumental Rock',
		'Ethnic',
		'Gothic',
		'Darkwave',
		'Techno-Industrial',
		'Electronic',
		'Pop-Folk',
		'Eurodance',
		'Dream',
		'Southern Rock',
		'Comedy',
		'Cult',
		'Gangsta',
		'Top 40',
		'Christian Rap',
		'Pop/Funk',
		'Jungle',
		'Native American',
		'Cabaret',
		'New Wave',
		'Psychadelic',
		'Rave',
		'Showtunes',
		'Trailer',
		'Lo-Fi',
		'Tribal',
		'Acid Punk',
		'Acid Jazz',
		'Polka',
		'Retro',
		'Musical',
		'Rock & Roll',
		'Hard Rock',
		'Folk',
		'Folk-Rock',
		'National Folk',
		'Swing',
		'Fast Fusion',
		'Bebob',
		'Latin',
		'Revival',
		'Celtic',
		'Bluegrass',
		'Avantgarde',
		'Gothic Rock',
		'Progressive Rock',
		'Psychedelic Rock',
		'Symphonic Rock',
		'Slow Rock',
		'Big Band',
		'Chorus',
		'Easy Listening',
		'Acoustic',
		'Humour',
		'Speech',
		'Chanson',
		'Opera',
		'Chamber Music',
		'Sonata',
		'Symphony',
		'Booty Bass',
		'Primus',
		'Porn Groove',
		'Satire',
		'Slow Jam',
		'Club',
		'Tango',
		'Samba',
		'Folklore',
		'Ballad',
		'Power Ballad',
		'Rhythmic Soul',
		'Freestyle',
		'Duet',
		'Punk Rock',
		'Drum Solo',
		'A capella',
		'Euro-House',
		'Dance Hall'
	);

	public static $metadata = array(
		'Title',                       // Title
		'Artist',                      // Artist
		'Album',                       // Album
		'Album Artist',                // Album Artist
		'Grouping',                    // Grouping
		'Year',                        // Year
		'Date',                        // Month and day of month
		'Time',                        // Hour and minute
		'Track',                       // Track number
		'Disk',                        // Disk number
		'Composer',                    // Composer
		'Comment',                     // Comment
		'Genre',                       // Genre
		'Genre Code',                  // Genre
		'Tempo',                       // BPM
		'Compilation',                 // Compilation
		'Cover',                       // Artwork
		'Rating',                      // Rating/Advisory
		'Category',                    // Category
		'Keywords',                    // Keyword
		'Description',                 // Description
		'Lyrics',                      // Lyrics
		'Encoder',                     // Encoder
		'Recording Copyright',         // Copyright
		'Copyright',                   // Copyright
		'Podcast',                     // Podcast
		'Podcast URL',                 // Podcast URL
		'Copyright URL',               // Copyright/Legal information
		'Audio Webpage',               // Official audio file webpage
		'Artist Webpage',              // Official artist/performer webpage
		'Audio Source Webpage',        // Official audio source webpage
		'Publisher Webpage',           // Official publisher webpage
		'Episode GUID',                // Episode Global Unique ID
		'TV Network Name',             // TV Network Name
		'TV Show Name',                // TV Show Name
		'TV Episode Number',           // TV Episode Number
		'TV Season',                   // TV Season
		'TV Episode',                  // TV Episode
		'Purchase Date',               // Purchase Date
		'Gapless Playback'             // Gapless Playback
	);

	public static function is_editable($path) {
		switch (pathinfo($path, PATHINFO_EXTENSION)) {
			case 'm4a':
			case 'mp4':
			case 'mp3':
				return true;
			default:
				return false;
		}
	}

	private static function get_parser($path) {
		switch (pathinfo($path, PATHINFO_EXTENSION)) {
			case 'm4a':
			case 'mp4':
				require_once 'format_mpeg4.php';
				return 'mp4parser';
			case 'mp3':
				require_once 'format_mp3.php';
				return 'mp3parser';
		}
		return null;
	}

	public static function parse($path) {
		if ($parserclass = self::get_parser($path)) {
			$parser = new $parserclass();
			return $parser->parse($path);
		} else {
			return null;
		}
	}

	public static function update($oldpath, $newpath, $metadata) {
		// read original file
		if (!($parserclass = self::get_parser($oldpath))) {
			return null;
		}
		$parser = new $parserclass();
		$oldcontainer = $parser->parse($oldpath);

		// clone original container
		$newcontainer = $oldcontainer->copy();

		// set metadata on new container
		$newcontainer->set_metadata($metadata);
		$newcontainer->update($oldcontainer);  // make adjustments on rest of file as necessary

		// merge existing and new data into new file
		$parser->merge($newcontainer, $newpath);

		return $newcontainer;
	}
}