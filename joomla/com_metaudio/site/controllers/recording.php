<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
* metaudio component recording controller
*/
class metaudioControllerRecording extends JControllerLegacy {
	public function __construct() {
		 parent::__construct();
		 // register extra tasks
		 $this->registerTask('edit', 'display');
	}

	/**
	* Authorizes access to the audio management interface.
	*/
	private function authorizeUser() {
		$user = JFactory::getUser();
		if ($user->guest) {  // only logged-in users are allowed to make edits
			$app = JFactory::getApplication();
			$app->enqueueMessage(JText::_('METAUDIO_ERROR_LOGIN_REQUIRED'), 'error');
			return false;
		} elseif (!$user->authorise('core.edit', 'com_metaudio')) {
			$app = JFactory::getApplication();
			$app->enqueueMessage(JText::_('METAUDIO_ERROR_PERMISSION_REQUIRED'), 'error');
			return false;
		} else {
			return true;
		}
	}
	
	/**
	* Displays the view.
	*/
	public function display($cachable = false, $urlparams = array()) {
		JRequest::setVar('view', 'recording');
		switch ($this->getTask()) {
			case 'edit':
				if ($this->authorizeUser()) {
					JRequest::setVar('layout', 'edit');
				} else {
					JRequest::setVar('layout', 'default');
				}
				break;
			default:
				JRequest::setVar('layout', 'default');
		}
		parent::display($cachable, $urlparams);
	}

	private function store() {
		$path = JRequest::getVar('f');
		if (isset($path)) {
			$newpath = JRequest::getVar('filename', $path);
			$metadata = JRequest::getVar('meta', array());

			// process uploaded files
			foreach ($_FILES as $postname => $postdata) {
				list($prefix, $key) = explode('-', $postname, 2);
				if ($prefix == 'meta' && $postdata['size'] > 0) {
					$metadata[$key] = array(
						'mime' => $postdata['type'],
						'file' => $postdata['tmp_name']
					);
				}
			}

			$model = $this->getModel('recording');
			$model->updateRecording($path, $newpath, $metadata);
		}
	}

	public function save() {
		if ($this->authorizeUser()) {  // check user permissions
			$this->store();
		}

		$menu = JSite::getMenu();
		$activemenu = $menu->getActive();
		if ($activemenu) {
			$menuid = $activemenu->id;
		}
		$this->setRedirect(JRoute::_('index.php?option=com_metaudio&Itemid='.$menuid, false));
	}

	public function apply() {
		if ($this->authorizeUser()) {  // check user permissions
			$this->store();
			JRequest::setVar('view', 'recording');
			JRequest::setVar('layout', 'edit');
		}

		parent::display();
	}

	public function cancel() {
		$menu = JSite::getMenu();
		$activemenu = $menu->getActive();
		if ($activemenu) {
			$menuid = $activemenu->id;
		}
		$this->setRedirect(JRoute::_('index.php?option=com_metaudio&Itemid='.$menuid, false));
	}
}