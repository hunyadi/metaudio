<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
* metaudio component controller
*/
class metaudioController extends JControllerLegacy {
	/**
	* Displays the view.
	*/
	public function display($cachable = false, $urlparams = array()) {
		parent::display($cachable, $urlparams);
	}
}