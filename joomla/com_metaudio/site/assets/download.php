<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

define('JPATH_BASE', dirname(dirname(dirname(dirname(__FILE__)))) );  // if download.php is in /portal/components/com_metaudio/assets, JPATH_BASE will be set to /portal
define('JPATH_COMPONENT', JPATH_BASE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_metaudio');
// phpinfo(INFO_VARIABLES); exit;

require_once JPATH_COMPONENT.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'filesystem.php';
require_once JPATH_BASE.DIRECTORY_SEPARATOR.'configuration.php';

/**
* Displays a custom critical HTTP 404 "Not Found" error message.
*/
function http_critical_error($message) {
	header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
	header('Status: 404 Not Found');
	header('Content-Type: text/html; charset=utf-8');
?>
<html>
<head>
<title>Audio not found</title>
</head>
<body>
<h1>Audio not found</h1>
<p><?php print $message; ?></p>
<hr/>
<p><address><a href="http://hunyadi.info.hu/projects/metaudio">metaudio Joomla extension</a><?php if (isset($_SERVER['HTTP_HOST'])) { print ' at '.$_SERVER['HTTP_HOST']; } ?></address></p>
</body>
<?php
	exit;
}

/**
* Displays a critical HTTP 404 "Not found" error message.
*/
function http_not_found($filename) {
	http_critical_error('The requested audio file '.($filename ? '<kbd>'.$filename.'</kbd> ' : '').'is not available on the server.');
}

/**
* Extracts audio relative URL from PATH_INFO.
*/
function http_path_info_url() {
	// extract path from URL
	if (isset($_SERVER['PATH_INFO']) && !empty($_SERVER['PATH_INFO'])) {
		$pathinfo = $_SERVER['PATH_INFO'];  // contains leading slash
	} elseif (isset($_SERVER['ORIG_PATH_INFO']) && !empty($_SERVER['ORIG_PATH_INFO'])) {
		$pathinfo = $_SERVER['ORIG_PATH_INFO'];
	} elseif (isset($_GET['f'])) {
		$pathinfo = $_GET['f'];
	} else {
		return false;
	}

	$self = basename(__FILE__);
	$selfstrpos = strpos($pathinfo, $self);  // some systems include download.php in PATH_INFO
	if ($selfstrpos !== false) {
		$url = substr($pathinfo, $selfstrpos + strlen($self));  // remove download.php
	} else {
		$url = $pathinfo;
	}
	return trim($url, '\\/');
}

// check hash string
if (!isset($_GET['h']))
	http_critical_error('The audio hash string to validate the download is missing from the request URL.');

// perform authentication if applicable
if (isset($_GET['a'])) {  // use Joomla authentication to check if user is logged in
	define('_JEXEC', 1);
	require_once JPATH_BASE.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'defines.php';
	require_once JPATH_BASE.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'framework.php';

	$mainframe =& JFactory::getApplication('site');
	$mainframe->initialise();

	$user =& JFactory::getUser();
	if (!$user->id)  // check if user is logged in
		http_critical_error('Listening to this audio recording requires authentication; you should log in to the website.');

	$userdata = $user->lastvisitDate;
} else {
	$userdata = '';
}

// normalize path to audio
$audiourl = http_path_info_url();
if (empty($audiourl))
	http_critical_error('The audio to download has not been specified in the URL.');

// check audio existence
$filename = basename($audiourl);
$audiopath = realpath(JPATH_BASE.DIRECTORY_SEPARATOR.str_replace('/', DIRECTORY_SEPARATOR, $audiourl));
if (!fsx::file_exists($audiopath))  // audio file not found
	http_not_found($filename);
if (substr($audiopath, 0, strlen(JPATH_BASE.DIRECTORY_SEPARATOR)) !== JPATH_BASE.DIRECTORY_SEPARATOR)  // audio path is outside Joomla folder
	http_not_found($filename);

// verify audio hash value
$jconfig = new JConfig();  // read Joomla configuration
$hash = md5($userdata.$audiopath.$jconfig->secret);  // use secret key to generate hashes more difficult to decypher
if ($hash != $_GET['h'])  // compare to computed hash
	http_not_found($filename);

$mime = 'application/octet-stream';
switch (pathinfo($filename, PATHINFO_EXTENSION)) {
	case 'm4a': $mime = 'audio/mp4'; break;
	case 'mp3': $mime = 'audio/mpeg'; break;
	case 'ogg': $mime = 'audio/ogg'; break;
}

// return audio as HTTP payload
header('Content-Type: '.$mime);
if ($filesize = fsx::filesize($audiopath)) {
	header('Content-Length: '.$filesize);
}
header('Content-Disposition: attachment; filename="'.$filename.'"');
@fsx::readfile($audiopath);
exit;