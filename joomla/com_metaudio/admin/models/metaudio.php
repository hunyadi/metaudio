<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

/**
* metaudio Model.
*/
class metaudioModelMetAudio extends JModelLegacy {
	/**
	* Discards all data in the audio metadata cache.
	*/
	public function cleanCache($group = null, $client_id = 0) {
		$db = JFactory::getDBO();
		$db->setQuery('DELETE FROM `#__metaudio_data`');
		$db->execute();
		$db->setQuery('DELETE FROM `#__metaudio_file`');
		$db->execute();
		$db->setQuery('DELETE FROM `#__metaudio_folder`');
		$db->execute();
	}
}
