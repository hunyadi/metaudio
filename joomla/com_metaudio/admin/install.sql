DROP TABLE IF EXISTS `#__metaudio_data`;
DROP TABLE IF EXISTS `#__metaudio_file`;
DROP TABLE IF EXISTS `#__metaudio_folder`;
DROP TABLE IF EXISTS `#__metaudio_property`;

CREATE TABLE IF NOT EXISTS `#__metaudio_property` (
	`propertyid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`propertyname` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`propertyid`),
	UNIQUE (`propertyname`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__metaudio_folder` (
	`folderid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`folderpath` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`folderid`),
	UNIQUE (`folderpath`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__metaudio_file` (
	`fileid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`folderid` INT UNSIGNED NOT NULL,
	`filename` VARCHAR(255) NOT NULL,
	`filetime` DATETIME NOT NULL,
	PRIMARY KEY (`fileid`),
	INDEX (`filename`),
	FOREIGN KEY (`folderid`) REFERENCES `#__metaudio_folder`(`folderid`) ON DELETE CASCADE
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__metaudio_data` (
	`fileid` INT UNSIGNED NOT NULL,
	`propertyid` INT UNSIGNED NOT NULL,
	`textvalue` VARCHAR(64000),
	PRIMARY KEY (`fileid`, `propertyid`),
	FOREIGN KEY (`fileid`) REFERENCES `#__metaudio_file`(`fileid`) ON DELETE CASCADE,
	FOREIGN KEY (`propertyid`) REFERENCES `#__metaudio_property`(`propertyid`) ON DELETE CASCADE
) DEFAULT CHARSET=utf8;

-- cachable metadata properties
INSERT INTO `#__metaudio_property` (`propertyname`)
VALUES
('Title'),
('Artist'),
('Album'),
('Album Artist'),
('Grouping'),
('Year'),
('Date'),
('Time'),
('Track'),
('Disk'),
('Composer'),
('Comment'),
('Genre'),
('Genre Code'),
('Tempo'),
('Compilation'),
('Cover'),
('Rating'),
('Category'),
('Keywords'),
('Description'),
('Lyrics'),
('Encoder'),
('Recording Copyright'),
('Copyright'),
('Podcast'),
('Podcast URL'),
('Copyright URL'),
('Audio Webpage'),
('Artist Webpage'),
('Audio Source Webpage'),
('Publisher Webpage'),
('Episode GUID'),
('TV Network Name'),
('TV Show Name'),
('TV Episode Number'),
('TV Season'),
('TV Episode'),
('Purchase Date'),
('Gapless Playback');