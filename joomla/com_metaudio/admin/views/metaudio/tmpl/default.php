<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

JToolBarHelper::title('metaudio', 'metaudio');
?>

<h1>Quick start</h1>

<p>In order to get started with metaudio, you need to create a front-end menu item for the extension:</p>

<ul>
<li>Open <em>Menus</em> in the top menu bar and select one of the existing front-end menus.</li>
<li>Click the icon <em>New</em> in the toolbar.</li>
<li>Click menu item type <em>metaudio</em>.</li>
<li>From the subtree that appears, select the leaf node <em>Default listing layout</em>.</li>
<li>Enter a caption for the audio file listing into the text box <em>Title</em>.</li>
<li>On the right-hand side, type the relative path of an existing folder into the text field for the parameter <em>Source folder</em>. The path is relative to the Joomla root folder. The extension has automatically created a folder with demo audio recordings upon installation, type <kbd>media/metaudio</kbd> to create a listing of these files.</li>
<li>Open the panel <em>Parameters (Component)</em> and adjust other parameter values as necessary.</li>
</ul>

<p>Once the menu item is created to show audio files from the specified folder, you can access the audio listing from the site front-end. You will be able to edit metadata only if you are logged in to the front-end.</p>

<p>For a complete documentation of features, visit the <a href="http://hunyadi.info.hu/projects/metaudio/">metaudio project website</a>.</p>

<form action="index.php" method="post" id="adminForm" name="adminForm">
<input type="hidden" name="option" value="com_metaudio" />
<input type="hidden" name="task" value="" />
</form>