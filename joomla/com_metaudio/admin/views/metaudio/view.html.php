<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
* HTML view class for the metaudio component back-end interface.
*/
class metaudioViewMetAudio extends JViewLegacy {
	public function display($tpl = null) {
		JToolBarHelper::trash('remove', JText::_('METAUDIO_BUTTON_CACHE'), false);
		JToolBarHelper::preferences('com_metaudio');
		$doc = JFactory::getDocument();
		$doc->addStyleSheet(JURI::base(true).'/components/com_metaudio/assets/css/metaudio.css');

		parent::display($tpl);
	}
}
