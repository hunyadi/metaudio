<?php
/**
* @file
* @brief    metaudio audio and music library
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2010 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/metaudio
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
* Installation helper for the extension.
*/
class com_metaudioInstallerScript {
		/**
		* Run when the componenet is to be installed.
		* @param $parent The object calling this method.
		* @return void
		*/
		public function install($parent) {
			$parent->getParent()->setRedirectURL('index.php?option=com_metaudio');  // redirect to metaudio welcome page
		}

		/**
		* Run when the component is to be uninstalled.
		* @return void
		*/
		public function uninstall($parent) { }

		/**
		* Run when the component is to be updated.
		* @return void
		*/
		public function update($parent) {
			$parent->getParent()->setRedirectURL('index.php?option=com_metaudio');  // redirect to metaudio welcome page
		}

		/**
		* Run before an install/update/uninstall operation takes place.
		* @param $parent is the class calling this method
		* @param $type The type of change ('install', 'update' or 'discover_install').
		* @return void
		*/
		public function preflight($type, $parent) { }

		/**
		* Run after an install/update/uninstall operation has taken place.
		* @param $parent is the class calling this method
		* @param $type The type of change ('install', 'update' or 'discover_install').
		* @return void
		*/
		public function postflight($type, $parent) { }
}