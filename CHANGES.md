# Full list of changes for metaudio audio and music library

Copyright 2010-2014 [Levente Hunyadi](http://hunyadi.info.hu/)

## 0.8.0.7

Added basic support for ID3v2 version 2.4 tags.
Added support for ID3v2 unsynchronization scheme when reading mp3 files.
Improved robustness of filesystem functions on Windows.
Fixed wrong URLs being generated for viewing and editing audio when URL rewriting is active.
Fixed display issues with genre when both a genre code and a genre name are present.

## 0.8.0.6

Added ability to set component configuration options for each menu item.
Added ability to associate an external image with each audio file (based on file or artist name).
Added new engine user interface options simple and advanced (with either MooTools or jQuery).

## 0.8.0.5

Fixed MooTools-based advanced player not playing audio on mobile devices.
Fixed an issue with the HTML 5 tag <audio> and Flash player support.

## 0.8.0.4

Fixed a compatibility issue related to MooTools.
Fixed the name "$" being used in place of "document.id" in JavaScript source, making the MooTools-based advanced player not play the song under some circumstances.

## 0.8.0.3

Added item sorting by last modified date.
Added RSS/Atom feed support for the list of items.
Fixed an error when component is triggered in a non-HTML context.
Enhanced download URL hash confidentiality.

## 0.8.0.1

Updated code to be fully compatible with Joomla 3.0.
Deprecated engines based on heavyweight SoundManager2.
Added rudimentary support for ogg streams.
Reorganized code, moving media files into separate media folder.
Fixed mild warning messages in more recent versions of PHP.

## 0.7

Added new component options to back-end to show or hide clock, peak volume meter, waveform spectrogram and status bar.
Restructured advanced graphical interface to be more object-oriented.
Migrated advanced graphical interface to native MooTools, eliminated dependency on external scripts jQuery and SWFObject.
Added language files for Spanish.

## 0.6

Added read support for obsoleted mp3 metadata format ID3v2.2.
Upgraded UTF-8 file system compatibility layer.
Fixed some translation placeholders not substituted with the translation text.
Fixed PHP warning messages when navigating to audio details view from search results.

## 0.5

Added search plug-in to find text in audio metadata.
Added audio player to top of audio details view.
Added play/pause button to advanced interface.

## 0.4

Added authorization for core.edit before allowing users to make changes to audio metadata.
Fixed PHP file system functions failing for filenames with non-ASCII characters in Windows.
Fixed player progress bar not displaying and seeking properly while audio is loading.

## 0.3

First public release.