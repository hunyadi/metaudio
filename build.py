import os
import os.path
import jbuild

def build(update):
	# minify javascript sources
	print('Minifying javascript files...')
	jbuild.minify(os.getcwd())

	# get (and update) version number
	version = jbuild.get_version(os.path.join(os.getcwd(), 'joomla', 'zip'), 'pkg_metaudio.xml', update)

	# package extension for Joomla 3.x distribution
	print('Packaging extension for Joomla 3.x...')

	# package component for distribution
	print('Packaging component...')
	jbuild.package(os.path.join(os.getcwd(), 'joomla', 'com_metaudio'), 'com_metaudio.ar.zip', version, os.path.join('joomla', 'zip'))

	# package search plug-in for distribution
	print('Packaging search plug-in...')
	jbuild.package(os.path.join(os.getcwd(), 'joomla', 'plg_search_metaudio'), 'plg_search_metaudio.ar.zip', version, os.path.join('joomla', 'zip'))

	# package distribution
	print('Packaging distribution...')
	jbuild.package(os.path.join(os.getcwd(), 'joomla', 'zip'), 'pkg_metaudio-' + version.as_string(), version, 'joomla')

reply = input('Update version number [y/n]?')
if reply.strip() in ['Yes','yes','Y','y']:
	print('Updating version number and repackaging...')
	build(True)
else:
	print('Repackaging...');
	build(False)
