# metaudio audio and music library

![metaudio](art/metaudio.png)

[metaudio](http://hunyadi.info.hu/projects/metaudio/) is a simple way to publish a folder with MP3 and MP4 (MPEG-4) music and audio recordings on a Joomla website. The extension extracts ID3v2 and Apple-style metadata from individual files, creates a listing featuring an audio player and showing the most important metadata (e.g. title, artist, album), and lets visitors view (and authenticated logged-in users edit) all other metadata. Supported metadata include title, artist, album, album artist, grouping, year, track and disk number, composer, comment, genre, tempo, encoder, copyright, JPEG or PNG cover art, description and lyrics. Metadata are cached in the database to improve performance and re-read automatically when files are updated either via the extension interface or the file system.

The primary goal of metaudio is to provide a non-commercial Joomla component that offers an intuitive yet versatile user interface to manage metadata of audio recordings. The current version supports viewing as well as editing Apple-style metadata in m4a (MPEG-4 audio) files, and ID3v2.2 (read only), ID3v2.3 (read and write) and (partially) ID3v2.4 metadata in mp3 files but future versions will support additional container formats.
