@echo off
if "%~1"=="" goto Usage

if exist "%TEMP%\jbuild" rmdir /q /s "%TEMP%\jbuild"
7za x "%~f1" "-o%TEMP%\jbuild"
del "%~f1"
7za a -tzip -mx=9 -mtc=off "%~f1" "%TEMP%\jbuild\*"
rmdir /q /s "%TEMP%\jbuild"
goto Quit

:Usage
echo Usage: %~nx0 c:\path\to\archive.zip
exit /b 1

:Quit
exit /b
